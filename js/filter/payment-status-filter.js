angular
    .module('flyrobe')
    .filter('paymentStatus', function () {
        return function (item) {

            var status      = {
                "ITD" : "INITIATED",
                "CLD" : "CANCELLED",
                "SCS" : "SUCCESS",
                "FLD" : "FAILED",
                "VER" : "VERIFIED"
            };

            return status[item];
        };
    });