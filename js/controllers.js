
/**
 * MainCtrl - controller
 * Contains several global data used in different view
 *
 */
function MainCtrl($rootScope,$http,$localStorage,$state,$scope,$filter) {
    mainVm=this;

    /**
     * slideInterval - Interval for bootstrap Carousel, in milliseconds:
     */
    this.slideInterval = 5000;

    mainVm.checkDefaultC2C          = checkDefaultC2C; //function to accept agreement
    mainVm.logout                   = logout; //function to logout
    mainVm.startTour                = startTour; //function to start tour
    mainVm.getUrl                   = getUrl;

    mainVm.checkDefaultC2C();//Default Call

    var date = new Date();
    mainVm.currentDate = $filter('date')(date, 'yyyy-MM-dd');

    mainVm.base_url = "https://ops.flyrobeapp.com"; //production
    // mainVm.base_url = "https://staging.flyrobeapp.com"; //staging

    mainVm.api                 = mainVm.base_url + "/api/web/v1/";


    mainVm.urlList          = {
                                    "url": "https://ops.flyrobeapp.com/", //production
                                    "merchant_url": "https://merchant.flyrobeapp.com/", //production
                                    "node_url" : "https://node.flyrobeapp.com:8002/", //production
                                    "node_url_2" : "https://node-web.flyrobeapp.com/", //production


                                    /*"url": "https://staging.flyrobeapp.com/", //staging
                                    "merchant_url": "http://node-staging.flyrobeapp.com:8080/", //staging
                                    "node_url" : "http://node-staging.flyrobeapp.com:8002/", //staging
                                    "node_url_2" : "http://node-staging.flyrobeapp.com:8001/", //staging*/

                                    "web_url" : "https://merchant.flyrobeapp.com/mms/img/",
                                    "website_url" : "https://flyrobe.com/",
                                    "imagekit_url" : "https://ik.imagekit.io/flyrobe/",
                                    "dashboard_1" : mainVm.base_url + "/api/dashboard/v1/",
                                    "cloudnary_url" : "https://ik.imagekit.io/flyrobe/"


                              };

    mainVm.header          = {
        'Authorization' : 'Bearer dashboard',
        'Content-Type'  : 'application/json'
    };
    mainVm.header1          = {
        'Authorization' : 'Bearer dashboard',
        'Content-Type'  : 'application/json'
    };
    mainVm.header2          = {
        'Authorization' : 'Bearer dashboard',
        'Content-Type'  : 'application/x-www-form-urlencoded'
    };

    if ($localStorage.merchant) {
        mainVm.token = $localStorage.merchant.access_token;
        mainVm.merchant_user_details = $localStorage.merchant.user_details;
        mainVm.header1.Authorization = 'Bearer ' + $localStorage.merchant.access_token;
        mainVm.header2.Authorization = 'Bearer ' + $localStorage.merchant.access_token;

        mainVm.config = $localStorage.merchant.config;
    }


    //take a tour ng-joyride
    var count = 0;
    mainVm.startJoyRide = false;

    function startTour () {
        count++;
        mainVm.startJoyRide = true;

    }


    /*$scope.config = [

        {
            type: "location_change",
            path: "/merchant/home"
        },
        {
            type: "title",
            heading: "1 of 6",
            text: '<div class="row"><div id="title-text" class="col-md-12"><h2>Hello</h2><span class="main-text">Welcome to <strong>Flyrobe\'s Vendor Management System !</strong></span><br><span>' +
            'We have compiled a quick tutorial that will walk you through the important sections.Please press next to continue or skip to End the tutorial.</span></div></div>',
            curtainClass: "randomClass"

        },
        {
            type: "element",
            selector: ".step2",
            heading: "2 of 6",
            text: '<div class="row"><div id="title-text" class="col-md-12">' +
            '<span class="main-text">' +
            'Home helps you to understand quick & holistic insights about your business with Flyrobe. Use this section ' +
            'if you want to understand which are the best/low performing products or if you want a sneak peak into the' +
            'total revenue generated via Flyrobe till date. ' +
            '</span></div></div>',
            placement: "right",
            curtainClass: "randomClass",
            attachToBody: true

        },
        {
            type: "location_change",
            path: "/merchant/orders"
        },
        {
            type: "element",
            selector: ".step3",
            heading: "3 of 6",
            text: '<div class="row"><div id="title-text" class="col-md-12">' +
            '<span class="main-text">' +
            'This section provides you the single view to find all your products listed with Flyrobe along with the ' +
            'revenue generated per product. Only serviced i.e. completed orders are used to calculate the revenue. ' +
            '</span></div></div>',
            placement: "right",
            curtainClass: "randomClass",
            attachToBody: true,
            scroll: true
        },
       /!* {
            type: "location_change",
            path: "/merchant/payment"
        },
        {
            type: "element",
            selector: ".step4",
            heading: "4 of 8",
            text: '<div class="row"><div id="title-text" class="col-md-12">' +
            '<span class="main-text">' +
            'Payments gives you complete view of your past payment history with Flyrobe as well as your upcoming payments.' +
            'Please note - Invoices are generated only at the end of each payment cycle & can be downloaded for reference.' +
            '</span></div></div>',
            placement: "right",
            curtainClass: "randomClass",
            attachToBody: true,
            scroll: true
        },*!/
        {
            type: "location_change",
            path: "/merchant/settings"
        },
        {
            type: "element",
            selector: ".step5",
            heading: "4 of 6",
            text: '<div class="row"><div id="title-text" class="col-md-12">' +
            '<span class="main-text">' +
            'You can use this section to change your logo or update your password.' +
            '</span></div></div>',
            placement: "right",
            curtainClass: "randomClass",
            attachToBody: true,
            scroll: true
        },
        {
            type: "location_change",
            path: "/merchant/home"
        },
        {
            type: "element",
            selector: ".step6",
            heading: "5 of 6",
            text: '<div class="row"><div id="title-text" class="col-md-12">' +
            '<span class="main-text">' +
            'This can used to be check product performance during different period of time.' +
            '</span></div></div>',
            placement: "bottom",
            curtainClass: "randomClass",
            attachToBody: true,
            scroll: true
        },
        {
            type: "element",
            selector: ".step7",
            heading: "6 of 6",
            text: '<div class="row"><div id="title-text" class="col-md-12">' +
            '<span class="main-text">' +
            'Clicking on this will give you details of each order for this product in positive selected time period.' +
            '</span></div></div>',
            placement: "bottom",
            curtainClass: "randomClass",
            attachToBody: true,
            scroll: true
        },
       /!* {
            type: "location_change",
            path: "/merchant/payment"
        },
        {
            type: "element",
            selector: ".step8",
            heading: "8 of 8",
            text: '<div class="row"><div id="title-text" class="col-md-12">' +
            '<span class="main-text">' +
            'This section shows the total outstanding receivables from Flyrobe. Invoices of which will be generated ' +
            'end of each cycle.' +
            '</span></div></div>',
            placement: "bottom",
            curtainClass: "randomClass",
            attachToBody: true,
            scroll: true
        },*!/

    ];

    if(mainVm.merchant_user_details && mainVm.merchant_user_details.type == 'C2C'){
        var temp =  {
                type: "location_change",
                path: "/merchant/assets-tracking"
            };
            var temp1 = {
                type: "element",
                selector: ".step9",
                heading: "7 of 8",
                text: '<div class="row"><div id="title-text" class="col-md-12">' +
            '<span class="main-text">' +
        'This section will help you track the status of items submitted to Flyrobe\'s Make Money Off Your Closet Programme.' +
        '</span></div></div>',
            placement: "bottom",
            curtainClass: "randomClass",
            attachToBody: true,
            scroll: true
    };
        $scope.config.push(temp);
        $scope.config.push(temp1);
    }

    {
        var temp =  {
            type: "location_change",
                path: "/merchant/home"
        };
        var temp1 = {
            type: "title",
                selector: ".step9",
            text: '<div class="row"><div id="title-text" class="col-md-12">' +
        '<span>' +
        'To view the tutorial again you can click on \'Take a Tour\'. <br> Thank You!' +
        '</span></div></div>',
            curtainClass: "randomClass"
        };
        $scope.config.push(temp);
        $scope.config.push(temp1);
    }*/


    function checkDefaultC2C() {
        if($localStorage.merchant && $localStorage.merchant.user_details &&
            $localStorage.merchant.user_details.type.indexOf('C2C') >= 0 &&
            $localStorage.merchant.user_details.is_contract_signed != true && $localStorage.merchant.user_details.is_contract_signed != 'true'){
            $localStorage.merchant = {};
        }
    }


    /**********************************************
     function to logout
     **********************************************/
    function logout(){
        delete $localStorage.merchant;
        $state.go('login');
    }

    function getUrl(url,id) {
        return mainVm.urlList.website_url + url.replace(/ /g,'-') + "/" + id;
    }

};


/**
 *
 * Pass all functions into module
 */
angular
    .module('flyrobe')
    .controller('MainCtrl', MainCtrl);







