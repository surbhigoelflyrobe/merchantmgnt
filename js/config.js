/**
 * FLYROBE - Responsive Dashboard
 *
 * FLYROBE theme use AngularUI Router to manage routing and views
 * Each view are defined as state.
 * Initial there are written state for all view in theme.
 *
 */
function config($stateProvider, $urlRouterProvider, $ocLazyLoadProvider, IdleProvider, KeepaliveProvider) {

    // Configure Idle settings
    IdleProvider.idle(5); // in seconds
    IdleProvider.timeout(120); // in seconds

    $urlRouterProvider.otherwise("/login");

    $ocLazyLoadProvider.config({
        // Set to true if you want to see what and when is dynamically loaded
        debug: false
    });

    $stateProvider

        .state('dashboards', {
            abstract: true,
            url: "/dashboards",
            templateUrl: "views/common/content.html?v=17102016-1"
        })
        .state('login', {
            url: "/login",
            templateUrl: "views/pages/login.html?v=20170123-3",
            data: { pageTitle: 'Merchant Mgnt System'},
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            // serie: true,
                            // name: 'parsley',
                            files: ['js/plugins/parsley/parsley.min.js']
                        },
                        {
                            files: ['js/plugins/sweetalert/sweetalert.min.js',
                                'css/plugins/sweetalert/sweetalert.css',
                                'css/sweet-alert-new.css']
                        },
                        {
                            name: 'oitozero.ngSweetAlert',
                            files: ['js/plugins/sweetalert/angular-sweetalert.min.js']
                        },
                        {
                            name: 'ngDialog',
                            files: ['css/plugins/ngDialog/ngDialog-theme-default.min.css',
                                'css/plugins/ngDialog/ngDialog.min.css',
                                'js/plugins/ngDialog/ngDialog.min.js']
                        }
                    ]);
                }
            }
        })
        .state('changePassword', {
            url: "/change-password",
            templateUrl: "views/pages/change-passowrd.html?v=20170123-1",
            data: { pageTitle: 'Merchant Mgnt System'},
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            // serie: true,
                            // name: 'parsley',
                            files: ['js/plugins/parsley/parsley.min.js']
                        },
                        {
                            files: ['js/plugins/sweetalert/sweetalert.min.js',
                                'css/plugins/sweetalert/sweetalert.css',
                                'css/sweet-alert-new.css']
                        },
                        {
                            name: 'oitozero.ngSweetAlert',
                            files: ['js/plugins/sweetalert/angular-sweetalert.min.js']
                        }
                    ]);
                }
            }
        })
        .state('recover', {
            url: "/recover-password",
            templateUrl: "views/pages/recover.html?v=20170119-1",
            data: { pageTitle: 'Merchant Mgnt System'},
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            files: ['js/plugins/parsley/parsley.min.js']
                        },
                        {
                            files: ['js/plugins/sweetalert/sweetalert.min.js',
                                'css/plugins/sweetalert/sweetalert.css',
                                'css/sweet-alert-new.css']
                        },
                        {
                            name: 'oitozero.ngSweetAlert',
                            files: ['js/plugins/sweetalert/angular-sweetalert.min.js']
                        }
                    ]);
                }
            }
        })
        .state('forgotResetPassword', {
            url: "/forgot-reset-password/:userId",
            templateUrl: "views/pages/forgot-reset.html?v=20170123-3",
            data: { pageTitle: 'Merchant Mgnt System'},
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            files: ['js/plugins/parsley/parsley.min.js']
                        },
                        {
                            files: ['js/plugins/sweetalert/sweetalert.min.js',
                                'css/plugins/sweetalert/sweetalert.css',
                                'css/sweet-alert-new.css']
                        },
                        {
                            name: 'oitozero.ngSweetAlert',
                            files: ['js/plugins/sweetalert/angular-sweetalert.min.js']
                        }
                    ]);
                }
            }
        })
        .state('resetPassword', {
            url: "/reset-password/:userId",
            templateUrl: "views/pages/reset-password.html?v=20170123-3",
            data: { pageTitle: 'Merchant Mgnt System'},
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            files: ['js/plugins/parsley/parsley.min.js']
                        },
                        {
                            files: ['js/plugins/sweetalert/sweetalert.min.js',
                                'css/plugins/sweetalert/sweetalert.css',
                                'css/sweet-alert-new.css']
                        },
                        {
                            name: 'oitozero.ngSweetAlert',
                            files: ['js/plugins/sweetalert/angular-sweetalert.min.js']
                        }
                    ]);
                }
            }
        })
        .state('merchant', {
            abstract: true,
            url: "/merchant",
            templateUrl: "views/common/content.html?v=17102016-1"
        })
        .state('merchant.home', {
            url: "/home",
            templateUrl: "views/merchant/home.html?v20170320-1",
            data: { pageTitle: 'Merchant Mgnt System'},
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            files: ['js/plugins/moment/moment.min.js']
                        },
                        {
                            serie: true,
                            files: ['js/plugins/dataTables/datatables.min.js','css/plugins/dataTables/datatables.min.css']
                        },
                        {
                            serie: true,
                            name: 'datatables',
                            files: ['js/plugins/dataTables/angular-datatables.min.js']
                        },
                        {
                            serie: true,
                            name: 'datatables.buttons',
                            files: ['js/plugins/dataTables/angular-datatables.buttons.min.js']
                        },
                        {
                            name: 'ui.select',
                            files: ['js/plugins/ui-select/select.min.js', 'css/plugins/ui-select/select.min.css']
                        },
                        {
                            serie: true,
                            files: ['js/plugins/daterangepicker/daterangepicker.js', 'css/plugins/daterangepicker/daterangepicker-bs3.css']
                        },
                        {
                            name: 'daterangepicker',
                            files: ['js/plugins/daterangepicker/angular-daterangepicker.js']
                        },
                        {
                            files: ['js/plugins/sweetalert/sweetalert.min.js', 'css/plugins/sweetalert/sweetalert.css']
                        },
                        {
                            name: 'oitozero.ngSweetAlert',
                            files: ['js/plugins/sweetalert/angular-sweetalert.min.js']
                        },
                        {
                            name: 'ngDialog',
                            files: ['css/plugins/ngDialog/ngDialog-theme-default.min.css',
                                'css/plugins/ngDialog/ngDialog.min.css',
                                'js/plugins/ngDialog/ngDialog.min.js']
                        }
                    ]);
                }
            }
        })
        .state('merchant.orders', {
            url: "/orders",
            templateUrl: "views/merchant/orders.html?v20170703-1",
            data: { pageTitle: 'Merchant Mgnt System' },
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            files: ['js/plugins/moment/moment.min.js']
                        },
                        {
                            serie: true,
                            files: ['js/plugins/dataTables/datatables.min.js','css/plugins/dataTables/datatables.min.css']
                        },
                        {
                            serie: true,
                            name: 'datatables',
                            files: ['js/plugins/dataTables/angular-datatables.min.js']
                        },
                        {
                            serie: true,
                            name: 'datatables.buttons',
                            files: ['js/plugins/dataTables/angular-datatables.buttons.min.js']
                        },
                        {
                            name: 'ui.select',
                            files: ['js/plugins/ui-select/select.min.js', 'css/plugins/ui-select/select.min.css']
                        },
                        {
                            serie: true,
                            files: ['js/plugins/daterangepicker/daterangepicker.js', 'css/plugins/daterangepicker/daterangepicker-bs3.css']
                        },
                        {
                            name: 'daterangepicker',
                            files: ['js/plugins/daterangepicker/angular-daterangepicker.js']
                        },
                        {
                            files: ['js/plugins/sweetalert/sweetalert.min.js',
                                'css/plugins/sweetalert/sweetalert.css',
                                'css/sweet-alert-new.css']
                        },
                        {
                            name: 'oitozero.ngSweetAlert',
                            files: ['js/plugins/sweetalert/angular-sweetalert.min.js']
                        },
                        {
                            name: 'ngDialog',
                            files: ['css/plugins/ngDialog/ngDialog-theme-default.min.css',
                                'css/plugins/ngDialog/ngDialog.min.css',
                                'js/plugins/ngDialog/ngDialog.min.js']
                        }
                    ]);
                }
            }
        })
     /*   .state('merchant.payment', {
            url: "/payment",
            templateUrl: "views/merchant/payment.html?v20170221-1",
            data: { pageTitle: 'Merchant Mgnt System' },
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            files: ['css/paymentCss.css']
                        },
                        {
                            files: ['js/plugins/moment/moment.min.js']
                        },
                        {
                            serie: true,
                            files: ['js/plugins/dataTables/datatables.min.js','css/plugins/dataTables/datatables.min.css']
                        },
                        {
                            serie: true,
                            name: 'datatables',
                            files: ['js/plugins/dataTables/angular-datatables.min.js']
                        },
                        {
                            serie: true,
                            name: 'datatables.buttons',
                            files: ['js/plugins/dataTables/angular-datatables.buttons.min.js']
                        },
                        {
                            name: 'ui.select',
                            files: ['js/plugins/ui-select/select.min.js', 'css/plugins/ui-select/select.min.css']
                        },
                        {
                            name: 'datePicker',
                            files: ['css/plugins/datapicker/angular-datapicker.css','js/plugins/datapicker/angular-datepicker.js']
                        },
                        {
                            files: ['js/plugins/sweetalert/sweetalert.min.js',
                                'css/plugins/sweetalert/sweetalert.css',
                                'css/sweet-alert-new.css']
                        },
                        {
                            name: 'oitozero.ngSweetAlert',
                            files: ['js/plugins/sweetalert/angular-sweetalert.min.js']
                        },
                        {
                            name: 'ngDialog',
                            files: ['css/plugins/ngDialog/ngDialog-theme-default.min.css',
                                'css/plugins/ngDialog/ngDialog.min.css',
                                'js/plugins/ngDialog/ngDialog.min.js']
                        }
                    ]);
                }
            }
        })
        .state('merchant.reports', {
            url: "/reports",
            templateUrl: "views/merchant/reports.html?v20170201-1",
            data: { pageTitle: 'Merchant Mgnt System' },
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            files: ['js/plugins/moment/moment.min.js']
                        },
                        {
                            serie: true,
                            files: ['js/plugins/dataTables/datatables.min.js','css/plugins/dataTables/datatables.min.css']
                        },
                        {
                            serie: true,
                            name: 'datatables',
                            files: ['js/plugins/dataTables/angular-datatables.min.js']
                        },
                        {
                            serie: true,
                            name: 'datatables.buttons',
                            files: ['js/plugins/dataTables/angular-datatables.buttons.min.js']
                        },
                        {
                            name: 'ui.select',
                            files: ['js/plugins/ui-select/select.min.js', 'css/plugins/ui-select/select.min.css']
                        },
                        {
                            name: 'datePicker',
                            files: ['css/plugins/datapicker/angular-datapicker.css','js/plugins/datapicker/angular-datepicker.js']
                        },
                        {
                            files: ['js/plugins/sweetalert/sweetalert.min.js', 'css/plugins/sweetalert/sweetalert.css']
                        },
                        {
                            name: 'oitozero.ngSweetAlert',
                            files: ['js/plugins/sweetalert/angular-sweetalert.min.js']
                        }
                    ]);
                }
            }
        })*/
        .state('merchant.assetsTracking', {
            url: "/assets-tracking",
            templateUrl: "views/merchant/assetsTracking.html?v20170201-1",
            data: { pageTitle: 'Merchant Mgnt System' },
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            files: ['css/assets-tracking.css']
                        },
                        {
                            files: ['js/plugins/moment/moment.min.js']
                        },
                        {
                            files: ['js/plugins/sweetalert/sweetalert.min.js', 'css/plugins/sweetalert/sweetalert.css']
                        },
                        {
                            name: 'oitozero.ngSweetAlert',
                            files: ['js/plugins/sweetalert/angular-sweetalert.min.js']
                        }
                    ]);
                }
            }
        })
        .state('merchant.faq', {
            url: "/faq",
            templateUrl: "views/merchant/faq.html?v20170201-1",
            data: { pageTitle: 'Merchant Mgnt System' },
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            files: ['css/faq.css']
                        }
                    ]);
                }
            }
        })
        .state('merchant.setting', {
            url: "/settings",
            templateUrl: "views/merchant/setting.html?v20170323-1",
            data: { pageTitle: 'Merchant Mgnt System' },
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            files: ['js/plugins/moment/moment.min.js']
                        },
                        {
                            serie: true,
                            files: ['js/plugins/dataTables/datatables.min.js','css/plugins/dataTables/datatables.min.css']
                        },
                        {
                            serie: true,
                            name: 'datatables',
                            files: ['js/plugins/dataTables/angular-datatables.min.js']
                        },
                        {
                            serie: true,
                            name: 'datatables.buttons',
                            files: ['js/plugins/dataTables/angular-datatables.buttons.min.js']
                        },
                        {
                            name: 'ui.select',
                            files: ['js/plugins/ui-select/select.min.js', 'css/plugins/ui-select/select.min.css']
                        },
                        {
                            name: 'datePicker',
                            files: ['css/plugins/datapicker/angular-datapicker.css','js/plugins/datapicker/angular-datepicker.js']
                        },
                        {
                            files: ['js/plugins/sweetalert/sweetalert.min.js',
                                'css/plugins/sweetalert/sweetalert.css',
                                'css/sweet-alert-new.css']
                        },
                        {
                            name: 'oitozero.ngSweetAlert',
                            files: ['js/plugins/sweetalert/angular-sweetalert.min.js']
                        }
                    ]);
                }
            }
        })
        .state('merchant.pickedUp', {
            url: "/picked-up",
            templateUrl: "views/merchant/particular-merchant/picked-up.html?v20170323-1",
            data: { pageTitle: 'Merchant Mgnt System' },
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            files: ['css/custom/picked-up.css']
                        },
                        {
                            files: ['js/plugins/moment/moment.min.js']
                        },
                        {
                            serie: true,
                            files: ['js/plugins/dataTables/datatables.min.js','css/plugins/dataTables/datatables.min.css']
                        },
                        {
                            serie: true,
                            name: 'datatables',
                            files: ['js/plugins/dataTables/angular-datatables.min.js']
                        },
                        {
                            serie: true,
                            name: 'datatables.buttons',
                            files: ['js/plugins/dataTables/angular-datatables.buttons.min.js']
                        },
                        {
                            name: 'ui.select',
                            files: ['js/plugins/ui-select/select.min.js', 'css/plugins/ui-select/select.min.css']
                        },
                        {
                            name: 'datePicker',
                            files: ['css/plugins/datapicker/angular-datapicker.css','js/plugins/datapicker/angular-datepicker.js']
                        },
                        {
                            files: ['js/plugins/sweetalert/sweetalert.min.js',
                                'css/plugins/sweetalert/sweetalert.css',
                                'css/sweet-alert-new.css']
                        },
                        {
                            name: 'oitozero.ngSweetAlert',
                            files: ['js/plugins/sweetalert/angular-sweetalert.min.js']
                        },
                        {
                            name: 'ngDialog',
                            files: ['css/plugins/ngDialog/ngDialog-theme-default.min.css',
                                'css/plugins/ngDialog/ngDialog.min.css',
                                'js/plugins/ngDialog/ngDialog.min.js']
                        }
                    ]);
                }
            }
        })
        .state('merchant.droppedOff', {
            url: "/dropped-off",
            templateUrl: "views/merchant/particular-merchant/drop-off.html?v20170323-1",
            data: { pageTitle: 'Merchant Mgnt System' },
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            files: ['css/custom/dropped-off.css']
                        },
                        {
                            files: ['js/plugins/moment/moment.min.js']
                        },
                        {
                            serie: true,
                            files: ['js/plugins/dataTables/datatables.min.js','css/plugins/dataTables/datatables.min.css']
                        },
                        {
                            serie: true,
                            name: 'datatables',
                            files: ['js/plugins/dataTables/angular-datatables.min.js']
                        },
                        {
                            serie: true,
                            name: 'datatables.buttons',
                            files: ['js/plugins/dataTables/angular-datatables.buttons.min.js']
                        },
                        {
                            name: 'ui.select',
                            files: ['js/plugins/ui-select/select.min.js', 'css/plugins/ui-select/select.min.css']
                        },
                        {
                            name: 'datePicker',
                            files: ['css/plugins/datapicker/angular-datapicker.css','js/plugins/datapicker/angular-datepicker.js']
                        },
                        {
                            files: ['js/plugins/sweetalert/sweetalert.min.js',
                                'css/plugins/sweetalert/sweetalert.css',
                                'css/sweet-alert-new.css']
                        },
                        {
                            name: 'oitozero.ngSweetAlert',
                            files: ['js/plugins/sweetalert/angular-sweetalert.min.js']
                        },
                        {
                            name: 'ngDialog',
                            files: ['css/plugins/ngDialog/ngDialog-theme-default.min.css',
                                'css/plugins/ngDialog/ngDialog.min.css',
                                'js/plugins/ngDialog/ngDialog.min.js']
                        }
                    ]);
                }
            }
        })
        .state('merchant.itemList', {
            url: "/item-list",
            templateUrl: "views/merchant/particular-merchant/item-list.html?v20180105-1",
            data: { pageTitle: 'Merchant Mgnt System' },
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            files: ['js/plugins/parsley/parsley.min.js']
                        },
                        {
                            files: ['js/plugins/moment/moment.min.js']
                        },
                        {
                            serie: true,
                            files: ['js/plugins/dataTables/datatables.min.js','css/plugins/dataTables/datatables.min.css']
                        },
                        {
                            serie: true,
                            name: 'datatables',
                            files: ['js/plugins/dataTables/angular-datatables.min.js']
                        },
                        {
                            serie: true,
                            name: 'datatables.buttons',
                            files: ['js/plugins/dataTables/angular-datatables.buttons.min.js']
                        },
                        {
                            name: 'ui.select',
                            files: ['js/plugins/ui-select/select.min.js',
                                'css/plugins/ui-select/select.min.css',
                                'css/plugins/ui-select/select2.css',
                                'css/plugins/ui-select/selectize.default.css']
                        },
                        {
                            name: 'datePicker',
                            files: ['css/plugins/datapicker/angular-datapicker.css','js/plugins/datapicker/angular-datepicker.js']
                        },
                        {
                            files: ['js/plugins/sweetalert/sweetalert.min.js',
                                'css/plugins/sweetalert/sweetalert.css',
                                'css/sweet-alert-new.css']
                        },
                        {
                            name: 'oitozero.ngSweetAlert',
                            files: ['js/plugins/sweetalert/angular-sweetalert.min.js']
                        },
                        {
                            name: 'ngDialog',
                            files: ['css/plugins/ngDialog/ngDialog-theme-default.min.css',
                                'css/plugins/ngDialog/ngDialog.min.css',
                                'js/plugins/ngDialog/ngDialog.min.js']
                        },
                        {
                            files: ['css/custom/item-list.css']
                        }
                    ]);
                }
            }
        })
        .state('merchant.placeOrder', {
            url: "/place-order",
            templateUrl: "views/merchant/partner-store/add-to-cart.html?v20180702-1",
            data: { pageTitle: 'Merchant Mgnt System' },
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            files: ['js/plugins/parsley/parsley.min.js']
                        },
                        {
                            files: ['js/plugins/moment/moment.min.js']
                        },
                        {
                            files: ['js/plugins/sweetalert/sweetalert.min.js',
                                'css/plugins/sweetalert/sweetalert.css',
                                'css/sweet-alert-new.css']
                        },
                        {
                            name: 'oitozero.ngSweetAlert',
                            files: ['js/plugins/sweetalert/angular-sweetalert.min.js']
                        },
                        {
                            files: ['css/custom/add-to-cart.css']
                        }
                    ]);
                }
            }
        })
        .state('merchant.salesReport', {
            url: "/sales-report",
            templateUrl: "views/merchant/partner-store/sales-report.html?v20180706-1",
            data: { pageTitle: 'Merchant Mgnt System' },
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            files: ['js/plugins/parsley/parsley.min.js']
                        },
                        {
                            files: ['js/plugins/moment/moment.min.js']
                        },
                        {
                            serie: true,
                            files: ['js/plugins/dataTables/datatables.min.js','css/plugins/dataTables/datatables.min.css']
                        },
                        {
                            serie: true,
                            name: 'datatables',
                            files: ['js/plugins/dataTables/angular-datatables.min.js']
                        },
                        {
                            serie: true,
                            name: 'datatables.buttons',
                            files: ['js/plugins/dataTables/angular-datatables.buttons.min.js']
                        },
                        {
                            serie: true,
                            files: ['js/plugins/daterangepicker/daterangepicker.js', 'css/plugins/daterangepicker/daterangepicker-bs3.css']
                        },
                        {
                            name: 'daterangepicker',
                            files: ['js/plugins/daterangepicker/angular-daterangepicker.js']
                        },
                        {
                            files: ['js/plugins/sweetalert/sweetalert.min.js',
                                'css/plugins/sweetalert/sweetalert.css',
                                'css/sweet-alert-new.css']
                        },
                        {
                            name: 'oitozero.ngSweetAlert',
                            files: ['js/plugins/sweetalert/angular-sweetalert.min.js']
                        },
                        {
                            name: 'ngDialog',
                            files: ['css/plugins/ngDialog/ngDialog-theme-default.min.css',
                                'css/plugins/ngDialog/ngDialog.min.css',
                                'js/plugins/ngDialog/ngDialog.min.js']
                        }
                    ]);
                }
            }
        })
        .state('merchant.viewInventory', {
            url: "/view-inventory",
            templateUrl: "views/merchant/partner-store/view-inventory.html?v20180706-1",
            data: { pageTitle: 'Merchant Mgnt System' },
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            files: ['css/custom/view-inventory.css']
                        },
                        {
                            files: ['js/plugins/parsley/parsley.min.js']
                        },
                        {
                            files: ['js/plugins/moment/moment.min.js']
                        },
                        {
                            files: ['js/plugins/sweetalert/sweetalert.min.js',
                                'css/plugins/sweetalert/sweetalert.css',
                                'css/sweet-alert-new.css']
                        },
                        {
                            name: 'oitozero.ngSweetAlert',
                            files: ['js/plugins/sweetalert/angular-sweetalert.min.js']
                        },
                        {
                            name: 'ngDialog',
                            files: ['css/plugins/ngDialog/ngDialog-theme-default.min.css',
                                'css/plugins/ngDialog/ngDialog.min.css',
                                'js/plugins/ngDialog/ngDialog.min.js']
                        },
                        {
                            files: ['css/custom/add-to-cart.css']
                        }
                    ]);
                }
            }
        });

}
angular
    .module('flyrobe')
    .config(config)
    .run(function($rootScope, $state) {
        $rootScope.$state = $state;
    });
