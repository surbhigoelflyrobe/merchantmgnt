/**
 * Created by Surbhi Goel on 02/07/18.
 */

function addToCart($scope,$http,$filter,$state,$localStorage,SweetAlert,
                        checkService) {

    if ($localStorage.merchant == "" || $localStorage.merchant == undefined || $localStorage.merchant == null ||
        $localStorage.merchant.access_token == "" || $localStorage.merchant.access_token == undefined || $localStorage.merchant.access_token == null) {
        $state.go('login');
        return;
    }

    ac = this;
    ac.spinner              = false;
    ac.show_image           = false;
    ac.show_invoice         = false;
    ac.show_otp_fields      = false;
    ac.disable_place_btn    = true;
    ac.disable_phone_no     = false;
    ac.active_tab           = 'SEARCH';
    ac.create               = {};
    ac.place_order          = {};
    ac.cart_arr             = [];
    ac.cart_id_arr          = [];
    ac.activated_code_msg   = "An activation code has been sent to your mobile.";



    ac.createSKU                = createSKU; // function to create SKU
    ac.searchSKUID              = searchSKUID; // function to search SKU
    ac.uploadImage              = uploadImage; // function to upload image
    ac.uploadImageOnBackend     = uploadImageOnBackend; // function to upload image on backend
    ac.addToCart                = addToCart; // function to add cart
    ac.placeOrder               = placeOrder; // function to place order
    ac.getOTP                   = getOTP; // function to get OTP & Verify it
    ac.resendOTP                = resendOTP; // function to resend OTP 
    ac.removeItemFromCart       = removeItemFromCart; // function to remove item from cart
    ac.getAccessToken           = getAccessToken;
    ac.getOTPFromBackend        = getOTPFromBackend;
    ac.verifyOTP                = verifyOTP;
    ac.getOTPForExistingUser    = getOTPForExistingUser;
    ac.loginNewCustomer         = loginNewCustomer;
    ac.getMerchantType          = getMerchantType;



    /***** If there is no tutorial *****/
    if(!mainVm.startJoyRide){

        if($localStorage.merchant.cart == "" || $localStorage.merchant.cart == undefined || $localStorage.merchant.cart == null){
            $localStorage.merchant.cart = [];
            $localStorage.merchant.cart_id_arr = [];
        }
        else{
            ac.cart_arr = $localStorage.merchant.cart;
            ac.cart_id_arr = $localStorage.merchant.cart_id_arr;
        }

        ac.getMerchantType($localStorage.merchant.user_details.merchant_id);

    }


    function uploadImageOnBackend(type) {

        ac.spinner = true;

        var formData = new FormData();
        if(type == 'add') {

            if (ac.create.image_arr && ac.create.image_arr.length) {
                formData.append('multi_count', ac.create.image_arr.length);
                for (var i = 0; i < ac.create.image_arr.length; i++) {
                    formData.append('multi_img_' + i, ac.create.image_arr[i]._file);
                }
            }
            else {
                formData.append('multi_count', 0);
                ac.createSKU();
                return false;
            }

        }
        else{

            if (ac.place_order.image_arr.length) {
                formData.append('multi_count', ac.place_order.image_arr.length);
                for (var i = 0; i < ac.place_order.image_arr.length; i++) {
                    formData.append('multi_img_' + i, ac.place_order.image_arr[i]._file);
                }
            }
            else {
                formData.append('multi_count', 0);
                formData.append('image', ac.place_order.image_arr[0]._file);
            }


        }

        $.ajax({
            type: 'POST',
            url: mainVm.urlList.node_url + 'image-upload-to-cloudinary',
            dataType: "json",
            data: formData,
            async: true,
            processData: false,
            contentType: false,
            success: function (response) {

                if(response.is_error){
                    ac.spinner = false;
                    SweetAlert.swal({
                        title: "",
                        text: "Something went wrong, please try again later."
                    });
                }
                else{

                    if(type == 'add'){
                        ac.createSKU(response.data);
                    }
                    else{
                        ac.placeOrder(response.data);
                    }

                }

                $scope.$apply();

            },
            error: function (response) {
            }
        });



    }
    
    
    function createSKU(image_url) {

        var images = [];
        if(image_url && image_url.length){
            image_url.forEach(function (col) {
                images.push(mainVm.urlList.imagekit_url + col);
            });
        }
        else{
           images = ["https://ik.imagekit.io/flyrobe/image/upload/v1530867541/tnucaqssvnzvsp3nkzvt.png"];
        }



        var send_json  = {
            merchant_id: "" + $localStorage.merchant.user_details.merchant_id,
            merchant_name: $localStorage.merchant.user_details.username,
            sku_code: ac.create.sku_id,
            mrp: ac.create.mrp,
            front_img: images[0],
            multi_img: images,
            back_img: '',
            short_description: ac.create.short_desc,
            lead_time: '',
            count_of_items: '',
            item_category_id: '',
            item_category_name: '',
            color: ''
        };

        $http({
            url: mainVm.urlList.node_url_2 + 'buy-back-catalog',
            method: "POST",
            data : send_json
        }).success(function (response) {

            if(response.is_error){
                SweetAlert.swal({
                    title: "",
                    text: "Something went wrong, please try again later."
                });
            }
            else{
                SweetAlert.swal({
                    title: "",
                    text: "Created Successfully."
                });
                ac.searchSKUID('create',ac.create.sku_id);
                $('.js-create-image-input').val("");
                ac.create = {};
            }
            ac.spinner = false;
        }).error(function (resposne) {
        });


    }


    function uploadImage(input,type) {

        if(type == 'add'){
            ac.create.image = input;
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('.js-image')
                        .attr('src', e.target.result)
                        .height(150);
                };

                reader.readAsDataURL(input.files[0]);
                ac.temp_file = input.files[0];
                ac.show_image = true;
                $scope.$apply();
            }
        }
        else{
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                if(ac.place_order == '' || ac.place_order == null || ac.place_order == undefined){
                    ac.place_order = {};
                }
                ac.place_order.image = input;

                reader.onload = function (e) {
                    $('.js-invoice-image')
                        .attr('src', e.target.result)
                        .height(50);
                };

                reader.readAsDataURL(input.files[0]);
                ac.show_invoice = true;
                $scope.$apply();
            }
        }

    }


    function searchSKUID(type,id) {

        ac.product_details = [];

        var id = (id ? id : ac.sku_id);

        if(type != 'create' && (ac.sku_id == '' || ac.sku_id == null || ac.sku_id == undefined)){
            SweetAlert.swal({
                title: "",
                text: "Please enter SKU id."
            });
        }
        else{
            ac.spinner = true;

            $http({
                url: mainVm.urlList.node_url_2 + 'buy-back-catalog?merchant_id=' + $localStorage.merchant.user_details.merchant_id +
                "&sku_code=" + id,
                method: "GET"
            }).success(function (response) {

                if(response.is_error){
                    SweetAlert.swal({
                        title: "",
                        text: "Something went wrong, please try again later."
                    });
                }
                else{

                    if(response.data.length){
                        if(type != 'create'){
                            ac.product_details = response.data;
                        }
                        else{
                            ac.addToCart(response.data[0]);//add to cart after creation
                        }

                    }
                    else{
                        SweetAlert.swal({
                            title: "",
                            text: ac.sku_id + " does not exist."
                        });
                    }
                }

                ac.spinner = false;
            }).error(function (resposne) {
            });
        }

    }


    function addToCart(item) {


        var temp = {
            id : item.id,
            sku_code : item.sku_code,
            mrp : item.mrp,
            front_img : item.front_img
        };

        var index = ac.cart_id_arr.indexOf(item.sku_code);

        if(index >= 0){
            ac.cart_arr[index].qty = ac.cart_arr[index].qty + 1;
        }
        else{
            temp.qty = 1;
            ac.cart_arr.push(temp);
            ac.cart_id_arr.push(item.sku_code);
            $localStorage.merchant.cart = ac.cart_arr;
            $localStorage.merchant.cart_id_arr = ac.cart_id_arr;
        }

        ac.product_details = [];
        ac.sku_id = "";

    }


    function placeOrder(image_url) {

        var images = [];
        image_url.forEach(function (col) {
            images.push(mainVm.urlList.imagekit_url + col);
        });

        var send_json  = {
            "merchant_id": $localStorage.merchant.user_details.merchant_id,
            "bill_value": parseFloat(ac.place_order.bill_value),
            "invoice_link": images,
            "user_id": 30001,
            "user_phone": ac.place_order.phone_no,
            "user_email": ac.place_order.email_id,
            "user_name": ac.place_order.customer_name,
            "user_info":{},
            "buy_back_pass_code": ac.place_order.pass_code,
            "cart_items":[]
        };

        ac.cart_arr.forEach(function (col) {
            var temp = {
                "buy_back_inventories_id": col.id,
                "mrp": col.mrp,
                "sku_code": col.sku_code,
                "number_of_quantity": parseInt(col.qty)
            };
            send_json.cart_items.push(temp);
        });




        $http({
            url: mainVm.urlList.node_url_2 + 'buy-back-orders',
            method: "POST",
            data : send_json
        }).success(function (response) {

            if(response.is_error){
                SweetAlert.swal({
                    title: "",
                    text: "Something went wrong, please try again later."
                });
            }
            else{
                SweetAlert.swal({
                    title: "",
                    text: "Order Placed Successfully."
                },function () {
                    $localStorage.merchant.cart = [];
                    $localStorage.merchant.cart_id_arr = [];
                    $state.reload();
                });
            }
            ac.spinner = false;
        }).error(function (resposne) {
        });
    }


    function getOTP() {

        var error = false;

        if(ac.place_order.customer_name == "" || ac.place_order.customer_name == null || ac.place_order.customer_name == undefined){
            error = true;
            ac.place_order.customer_name_error = true;
        }

        if(ac.place_order.email_id == "" || ac.place_order.email_id == null || ac.place_order.email_id == undefined){
            error = true;
            ac.place_order.email_id_error = true;
        }

        if(ac.place_order.phone_no == "" || ac.place_order.phone_no == null || ac.place_order.phone_no == undefined || ac.place_order.phone_no.length < 10){
            error = true;
            ac.place_order.phone_no_error = true;
        }

        if(!error){
            ac.spinner = true;
            ac.getAccessToken();
        }

    }

    function getAccessToken() {

        $http({
            url : mainVm.api + 'guest-user/access-token/',
            method: "GET"
        }).success(function (response, status, headers, config) {

            $localStorage.merchant.guest_access_token = response.data.guest_access_token;
            ac.getOTPFromBackend();

        });

    }


    function getOTPFromBackend() {

        ac.register_data = "";

        $http({
            url : mainVm.urlList.dashboard_1 + 'users/',
            method: "POST",
            header : {
                'Authorization' : 'Bearer ' + $localStorage.merchant.guest_access_token,
                'Content-Type'  : 'application/json'
            },
            data : {
                "name": ac.place_order.customer_name,
                "mobile": ac.place_order.phone_no,
                "email": ac.place_order.email_id,
                "password": "sgtryvh6565rfc",
                "re_enter_password": "sgtryvh6565rfc"
            }
        }).success(function (response, status, headers, config) {

            if(!response.is_error){
                ac.spinner = false;
                ac.register_data = response.data;
                ac.show_otp_fields = true;
                SweetAlert.swal({
                    title: "",
                    text: ac.activated_code_msg
                });
            }


        }).error(function (response, status, headers, config) {

            if(status == 400){
                if(response.data.email){
                    SweetAlert.swal({
                        title: "",
                        text: "Please enter another email-id."
                    });
                }
                else if(response.data.mobile){
                    ac.getOTPForExistingUser(ac.place_order.phone_no);
                }
            }

            ac.spinner = false;

        });
    }

    function verifyOTP() {

        var error = false;

        if(ac.place_order.otp == "" || ac.place_order.otp == null || ac.place_order.otp == undefined){
            error = true;
            ac.place_order.otp_error = true;
        }

        if(!error){

            if(ac.register_data == ""){ //for existing user

                $http({
                    url :  mainVm.api + 'users-otp/verify-otp/',
                    method: "POST",
                    headers : {
                        'Authorization' : 'Bearer ' + $localStorage.merchant.guest_access_token,
                        'Content-Type'  : 'application/json'
                    },
                    data : {
                        "mobile": ac.place_order.phone_no,
                        "activation_code": ac.place_order.otp
                    }
                }).success(function (response, status, headers, config) {

                    if(!response.is_error){
                        ac.disable_place_btn = false;
                        ac.disable_phone_no = true;
                    }
                    else{
                        SweetAlert.swal({
                            title: "",
                            text: response.data.activation_code
                        });
                    }

                }).error(function (response, status, headers, config) {

                    if(status == 400){//incorrect OTP
                        SweetAlert.swal({
                            title: "",
                            text: "The provided activation code is incorrect. Please check and try again."
                        });
                    }
                    else if(status == 404){
                        SweetAlert.swal({
                            title: "",
                            text: "Not Found"
                        });
                    }
                    else{
                        SweetAlert.swal({
                            title: "",
                            text: "The provided activation code is incorrect. Please check and try again."
                        });
                    }



                });
            }
            else{ // new users
                $http({
                    url :  mainVm.base_url + ac.register_data.links.verify_activation_code,
                    method: "PATCH",
                    header : {
                        'Authorization' : 'Bearer ' + $localStorage.merchant.guest_access_token,
                        'Content-Type'  : 'application/json'
                    },
                    data : {
                        activation_code : ac.place_order.otp
                    }
                }).success(function (response, status, headers, config) {

                    if(!response.is_error){
                        ac.disable_place_btn = false;
                        ac.disable_phone_no = true;
                        ac.loginNewCustomer();
                    }

                }).error(function (response, status, headers, config) {

                    if(status == 400){
                        SweetAlert.swal({//incorrect OTP
                            title: "",
                            text: "The provided activation code is incorrect. Please check and try again."
                        });
                    }
                    else{
                        SweetAlert.swal({
                            title: "",
                            text: "Something went wrong."
                        });
                    }



                });
            }

        }

    }


    function loginNewCustomer() {

        var temp = $.param({
            grant_type: "password",
            mobile: ac.place_order.phone_no,
            password: "sgtryvh6565rfc",
            fly_guest_token: $localStorage.merchant.guest_access_token
        });

        $http({
            url :  mainVm.base_url + '/o/token/mobile/',
            method: "POST",
            headers : {
                'Authorization' : 'Basic ' + "M0JiU0NrendTbW9ZZWNHVnNHaUp4cFVDV3YzVjFCOExMOUNINjE4YTpnZzVKSlNqNVhpVkxVcHFtTVB5bGQ3YjNpblVPVkhleEtDM0o2RlVRcDIyZjJrZkU5Y3BGME1SQzVYYUZDd3hWWG1pcmZrYjdvVkZidTVHNXUxN0RFb3F3dzAwVnI5dHd4V2lDV1o5Y1NMVDF3UkhLZU1BNGNJZElvcFdxUWVhZQ==",
                'Content-Type'  : 'application/x-www-form-urlencoded'
            },
            data : temp
        }).success(function (response, status, headers, config) {

        }).error(function (response, status, headers, config) {
        });

    }

    function getOTPForExistingUser(phone_no) {

        $http({
            url : mainVm.api + 'users-otp/create-otp/',
            method: "POST",
            header : {
                'Authorization' : 'Bearer ' + $localStorage.merchant.guest_access_token,
                'Content-Type'  : 'application/json'
            },
            data : {
                "mobile": phone_no
            }
        }).success(function (response, status, headers, config) {

            if(!response.is_error){
                SweetAlert.swal({
                    title: "",
                    text: ac.activated_code_msg
                });
                ac.show_otp_fields = true;
            }

        });

    }
    
    
    function resendOTP() {
        if(ac.register_data == ""){ //for existing user

            $http({
                url :  mainVm.api + 'users-otp/resend-otp/',
                method: "POST",
                headers : {
                    'Authorization' : 'Bearer ' + $localStorage.merchant.guest_access_token,
                    'Content-Type'  : 'application/json'
                },
                data : {
                    "mobile": ac.place_order.phone_no
                }
            }).success(function (response, status, headers, config) {

                if(!response.is_error){
                    SweetAlert.swal({
                        title: "",
                        text: ac.activated_code_msg
                    });
                }
                else{
                    SweetAlert.swal({
                        title: "",
                        text: response.data.activation_code
                    });
                }

            }).error(function (response, status, headers, config) {

                if(status == 400){//incorrect OTP
                    SweetAlert.swal({
                        title: "",
                        text: "The provided activation code is incorrect. Please check and try again."
                    });
                }
                else if(status == 404){
                    SweetAlert.swal({
                        title: "",
                        text: "Not Found"
                    });
                }
                else{
                    SweetAlert.swal({
                        title: "",
                        text: "The provided activation code is incorrect. Please check and try again."
                    });
                }



            });
        }
        else{ // new users
            $http({
                url :  mainVm.base_url + ac.register_data.links.resend_activation_code,
                method: "PATCH",
                header : {
                    'Authorization' : 'Bearer ' + $localStorage.merchant.guest_access_token,
                    'Content-Type'  : 'application/json'
                },
                data : {
                    user : ac.place_order.phone_no
                }
            }).success(function (response, status, headers, config) {

                if(!response.is_error){
                    SweetAlert.swal({
                        title: "",
                        text: ac.activated_code_msg
                    });
                }

            }).error(function (response, status, headers, config) {

                if(status == 400){
                    SweetAlert.swal({//incorrect OTP
                        title: "",
                        text: "The provided activation code is incorrect. Please check and try again."
                    });
                }
                else{
                    SweetAlert.swal({
                        title: "",
                        text: "Something went wrong."
                    });
                }



            });
        }
    }


    function removeItemFromCart(item) {

        var temp = {
            id : item.id,
            sku_code : item.sku_code,
            mrp : item.mrp,
            front_img : item.front_img
        };

        var index = ac.cart_id_arr.indexOf(item.sku_code);

        if(index != -1){
            ac.cart_arr.splice(index,1);
            ac.cart_id_arr.splice(index,1);
            $localStorage.merchant.cart = ac.cart_arr;
            $localStorage.merchant.cart_id_arr = ac.cart_id_arr;

        }

    }

    function getMerchantType(merchant_id) {
        ac.spinner  = true;

        $http({
            url: mainVm.urlList.node_url_2 + 'offline-designers/all?merchant_id='+ merchant_id,
            method: "GET"
        }).success(function (response) {

            if(response.is_error){
                SweetAlert.swal({
                    title: "",
                    text: "Something went wrong, please try again later."
                });
            }
            else{

                /*if(response.data[0].type == "designer"){
                    ac.show_add_sku = false;
                }
                else{
                    ac.show_add_sku = true;
                }*/
            }
            ac.spinner = false;
        }).error(function (resposne) {
        });


    }

}


angular
    .module('flyrobe')
    .controller('addToCart', addToCart);