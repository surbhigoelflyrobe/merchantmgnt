/**
 * Created by Surbhi Goel on 06/07/18.
 */

function salesReportCtrl($scope, DTOptionsBuilder, $http, $filter, $state, $localStorage, SweetAlert, ngDialog,
                         checkService) {

    if ($localStorage.merchant == "" || $localStorage.merchant == undefined || $localStorage.merchant == null ||
        $localStorage.merchant.access_token == "" || $localStorage.merchant.access_token == undefined || $localStorage.merchant.access_token == null) {
        $state.go('login');
        return;
    }

    sr = this;
    sr.spinner = false; 
    sr.invoice_list     = [];
    sr.total_revenue    = 0;
    mainVm.ctlr = 'SALES';

    sr.dtOptions = DTOptionsBuilder.newOptions()
        .withDisplayLength(50)
        //.withOption('order', [1, 'asc'])//desc
        .withDOM('<"html5buttons"B>lTfgitp')
        .withButtons([
            {extend: 'copy'}
        ]);


    sr.getData              = getData;
    sr.openModalForImage    = openModalForImage;
    sr.setDate              = setDate;


    /***** If there is no tutorial *****/
    if(!mainVm.startJoyRide){

        if($localStorage.merchant.sales && $localStorage.merchant.sales.start && $localStorage.merchant.sales.end){
            sr.setDate($localStorage.merchant.sales.start,$localStorage.merchant.sales.end);
        }
        else{

            $localStorage.merchant.sales = {};

            //get last month data
            var start = new Date();
            start.setMonth(start.getMonth() - 1);
            start = $filter('date')(new Date(start),'dd-MMM-yyyy');
            var end = $filter('date')(new Date(),'dd-MMM-yyyy');
            sr.setDate(start,end);
        }

    }


    function getData(start_date,end_date) {

        sr.spinner = true;
        $localStorage.merchant.sales.start = start_date;
        $localStorage.merchant.sales.end   = end_date;

        $http({
            url: mainVm.urlList.node_url_2 + 'buy-back-orders?merchant_id=' + parseInt($localStorage.merchant.user_details.merchant_id) +
            '&from_date=' + start_date + '&to_date=' + end_date,
            method: "GET"
        }).success(function (response) {

            sr.spinner = false;

            if(response.is_error){
                SweetAlert.swal({
                    title: "",
                    text: "Something went wrong, please try again later."
                });
            }
            else{
                sr.invoice_list = response.data;
                sr.total_revenue = 0;

                sr.invoice_list.forEach(function (col) {
                    sr.total_revenue += col.bill_value;
                });

                var s = $filter('date')(new Date(start_date),'dd-MMM-yyyy');
                var e = $filter('date')(new Date(end_date),'dd-MMM-yyyy');

                $("#rangeInput").val(s + " - " + e);

            }
        }).error(function (resposne) {
        });

    }

    function openModalForImage(image_url) {

        sr.image_path = image_url;

        ngDialog.open({
            template: 'imageModals',
            className: 'ngdialog-theme-default',
            showClose: false,
            scope: $scope
        });

    }

    function setDate(start_date,end_date) {

        sr.getData($filter('date')(new Date(start_date),'yyyy-MM-dd'),$filter('date')(new Date(end_date),'yyyy-MM-dd'));

    }

}



angular
    .module('flyrobe')
    .controller('salesReportCtrl', salesReportCtrl);