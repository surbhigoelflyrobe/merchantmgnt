/**
 * Created by Surbhi Goel on 06/07/18.
 */

function viewInventoryCtrl($scope,$http,$filter,$state,$localStorage,SweetAlert,ngDialog) {

    if ($localStorage.merchant == "" || $localStorage.merchant == undefined || $localStorage.merchant == null ||
        $localStorage.merchant.access_token == "" || $localStorage.merchant.access_token == undefined || $localStorage.merchant.access_token == null) {
        $state.go('login');
        return;
    }

    vi = this;
    vi.spinner      = false;
    vi.sku_list     = [];


    vi.getData              = getData;
    vi.openModalForImage    = openModalForImage;


    /***** If there is no tutorial *****/
    if(!mainVm.startJoyRide){
        vi.getData();//Default Call
    }


    function getData() {

        vi.spinner = true;

        $http({
            url: mainVm.urlList.node_url_2 + 'buy-back-catalog?merchant_id=' + parseInt($localStorage.merchant.user_details.merchant_id),
            method: "GET"
        }).success(function (response) {

            vi.spinner = false;

            if(response.is_error){
                SweetAlert.swal({
                    title: "",
                    text: "Something went wrong, please try again later."
                });
            }
            else{
                vi.sku_list = response.data;
            }

        }).error(function (resposne) {
        });

    }

    function openModalForImage(image_url) {

        vi.image_path = image_url;

        ngDialog.open({
            template: 'imageModals',
            className: 'ngdialog-theme-default',
            showClose: false,
            scope: $scope
        });

    }

}



angular
    .module('flyrobe')
    .controller('viewInventoryCtrl', viewInventoryCtrl);