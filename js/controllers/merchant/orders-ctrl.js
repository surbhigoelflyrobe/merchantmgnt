/**
 * Created by Surbhi Goel on 01/02/17.
 */

function ordersCtrl($scope,DTOptionsBuilder,$http,$filter,$state,$localStorage,SweetAlert,ngDialog) {

    if ($localStorage.merchant == "" || $localStorage.merchant == undefined || $localStorage.merchant == null ||
        $localStorage.merchant.access_token == "" || $localStorage.merchant.access_token == undefined || $localStorage.merchant.access_token == null) {
        $state.go('login');
        return;
    }

    order = this;
    order.spinner = false;
    order.data = false;
    order.selectedTab = 'ALL';
    order.data = true;
    mainVm.ctlr = 'ORDER';

    order.getData = getData;
    order.getTurns = getTurns;
    order.printTable = printTable;

    order.dtOptions     =   DTOptionsBuilder.newOptions()
        .withDisplayLength(50)
        //.withOption('order', [1, 'asc'])//desc
        .withDOM('<"html5buttons"B>lTfgitp')
        .withButtons([
            {extend: 'copy'}
        ]);

    if(!mainVm.startJoyRide){

        if($localStorage.merchant.order && $localStorage.merchant.order.start && $localStorage.merchant.order.end){
            order.getData($localStorage.merchant.order.start,$localStorage.merchant.order.end);
        }
        else{

            //get last month data
            var end = new Date();
            end.setMonth(end.getMonth() + 4);
            var start = $filter('date')(new Date(),'dd-MMM-yyyy');
            end = $filter('date')(end,'dd-MMM-yyyy');
            order.getData(start,end);
        }
    }


    function getData(start,end) {

        order.start = start;
        order.end = end;

        $localStorage.merchant.order = {
            start : start,
            end : end
        }

        if(start == '' || start == null || start == undefined ||
            end == '' || end == null || end == undefined){
            SweetAlert.swal({
                title: "",
                text: "Please select date"
            });
            return false;
        }


        order.spinner = true;
        order.data = true;

        var data = $.param({
            "user_token" : mainVm.token,
            "from_date" : order.start,
            "to_date" : order.end,
            "type" : order.selectedTab
        });

        $http({
            url: mainVm.urlList.node_url + 'merchant-outfit-list/',
            method: "POST",
            data: data,
            headers : {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function (response) {

            if(response.is_error){
                SweetAlert.swal({
                    title: "",
                    text: response.message
                },function () {
                    $state.go('login');
                });
            }
            else{
                order.tableData = response.data;
                order.spinner = false;
                var value = $filter('date')(new Date(start),'yyyy-MM-dd') + ' - ' + $filter('date')(new Date(),'yyyy-MM-dd');
                $("#rangeInput").val(start + " - " + end);
            }



        }).error(function (response) {
        });


    }


    function getTurns(noOfOrders,wStackId) {
        order.spinner = true;

        if(noOfOrders == 0){
            order.spinner = false;
            SweetAlert.swal({
                title: "",
                text: "Not Available"
            });
        }
        else{

            $http({
                url: mainVm.urlList.node_url + 'merchant-order-item-info-with-new-db',
                method: "POST",
                data :
                    $.param({
                        "user_token": mainVm.token,
                        "from_date" : order.start,
                        "to_date" : order.end,
                        "ware_house_id" : wStackId
                    }),
                headers : {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (response) {

                if(response.is_error){
                    SweetAlert.swal({
                        title: "",
                        text: response.message
                    },function () {
                        $state.go('login');
                    });
                }
                else{
                    order.turnArray = response.data;
                    order.spinner = false;
                    ngDialog.open({
                        template: 'turnsModals',
                        className: 'ngdialog-theme-default order-dialogue',
                        showClose: false,
                        scope: $scope,
                        preCloseCallback: function () {
                            return true;
                        }
                    });
                }

            }).error(function (response) {
            });
        }

    }


    function printTable() {

        var printSection = document.getElementById('printSection');

        // if there is no printing section, create one
        if (!printSection) {
            printSection = document.createElement('div');
            printSection.id = 'printSection';
            document.body.appendChild(printSection);
        }
        var domClone = $(".printTable").html();
        $(printSection).html(domClone);
        window.print();
        $(printSection).html("");

    }

}

angular
    .module('flyrobe')
    .controller('ordersCtrl', ordersCtrl);