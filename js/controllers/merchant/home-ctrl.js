/**
 * Created by Surbhi Goel on 01/02/17.
 */

function homeCtrl($scope,DTOptionsBuilder,$http,$filter,$state,$localStorage,SweetAlert,ngDialog) {

    if ($localStorage.merchant == "" || $localStorage.merchant == undefined || $localStorage.merchant == null ||
            $localStorage.merchant.access_token == "" || $localStorage.merchant.access_token == undefined || $localStorage.merchant.access_token == null) {
        $state.go('login');
        return;
    }

    home = this;

    home.dtOptions     =   DTOptionsBuilder.newOptions()
        .withDisplayLength(50)
        //.withOption('order', [1, 'asc'])//desc
        .withDOM('<"html5buttons"B>lTfgitp')
        .withButtons([
            {extend: 'copy'}
        ]);

    //flag
    home.spinners = false;
    home.datespinners = false;
    home.range = '';
    mainVm.ctlr = 'HOME';

    home.getData = getData;
    home.searchDataAccToDate = searchDataAccToDate;
    home.searchDate = searchDate;
    home.getTurns = getTurns;

    if(mainVm.startJoyRide){
        home.spinners = true;
        home.range = "123";
    }
    else
        home.getData(); //default call

    if($localStorage.merchant.user_details){
        home.user_details = $localStorage.merchant.user_details;
    }



    function getData() {

        var data = $.param({"user_token" : mainVm.token});

        $http({
            url: mainVm.urlList.node_url + 'merchant-summary/',
            method: "POST",
            data: data,
            headers : {'Content-Type': 'application/x-www-form-urlencoded'}
            // headers: mainVm.headers
        }).success(function (response) {

            home.adminData = response.data.merchantSummaryInfo[0];
            home.totalWorth = response.data.merchantTotalWorthInfo[0].outfit_total_worth;

           if(response.is_error){
               SweetAlert.swal({
                   title: "",
                   text: response.message
               },function () {
                   $state.go('login');
               });
           }
           else{
               if(home.adminData.rental_revenue != null)
                   home.adminData.rental_revenue = parseFloat(home.adminData.rental_revenue).toFixed(2);
               else
                   home.adminData.rental_revenue = 0.00;

               if(home.totalWorth == null)
                   home.totalWorth = 0.00;

               //get summary of last month
               var lastMonth = new Date();
               lastMonth.setMonth(lastMonth.getMonth()-3);
               var start = $filter('date')(lastMonth,'dd-MMM-yyyy');
               var end = $filter('date')(new Date(),'dd-MMM-yyyy');
               home.searchDataAccToDate(start,end,lastMonth);
           }

        }).error(function (response) {

        });

    }


    function searchDate(start,end) {

        if(end && start){

            home.datespinners = true;
            // var res = $("#rangeInput").val();
            // res = res.split(" ");
            // var start = $filter('date')(new Date(res[0]),'dd-MMM-yyyy');
            // var end = $filter('date')(new Date(res[2]),'dd-MMM-yyyy');

            // start = $filter('date')(new Date(start),'dd-MMM-yyyy');
            // end = $filter('date')(new Date(end),'dd-MMM-yyyy');

            home.searchDataAccToDate(start,end);

        }
        else{
            SweetAlert.swal({
                title: "",
                text: "Please select date"
            });
        }

    }


    function searchDataAccToDate(start,end,lastMonth) {

        home.start = start;
        home.end = end;

        home.range = start;

        var data = $.param({
            "user_token" : mainVm.token,
            "from_date" : start,
            "to_date" : end
        });

        $http({
            url: mainVm.urlList.node_url + 'merchant-summary-assortment/',
            method: "POST",
            data: data,
            headers : {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function (response) {

            if(response.is_error){
                SweetAlert.swal({
                    title: "",
                    text: response.message
                },function () {
                    $state.go('login');
                });
            }
            else{

                home.summary = {};
                home.summary.categories = [];
                home.summary.top_performing_outfits = response.data.top_performing_outfits;
                home.summary.bottom_performing_outfits = response.data.bottom_performing_outfits;

                for(var count in response.data.categories){
                    var data = {
                        category : response.data.categories[count].category,
                        no_of_items : response.data.categories[count].no_of_items,
                        no_of_orders : response.data.categories[count].no_of_orders
                    };
                    home.summary.categories.push(data);
                }

                console.log("start " + start);
                console.log("end " + end);

                //set value of input
                // start = new Date();
                // start.setMonth(start.getMonth()-1);
                // var value = $filter('date')(new Date(start),'yyyy-MM-dd') + ' - ' + $filter('date')(new Date(),'yyyy-MM-dd');
                if($("#rangeInput").val() == '' || $("#rangeInput").val() == null || $("#rangeInput").val() == undefined)
                    $("#rangeInput").val(start + ' - ' + end);

                home.datespinners = false;
                home.spinners = true;

            }

        }).error(function (response) {
        });

    }

    function getTurns(noOfOrders,wStackId) {

        if(noOfOrders == 0){
            SweetAlert.swal({
                title: "",
                text: "Not Available"
            });
        }
        else{

            $http({
                url: mainVm.urlList.node_url + 'merchant-order-item-info-with-new-db',
                method: "POST",
                data :
                    $.param({
                        "user_token": mainVm.token,
                        "from_date" : home.start,
                        "to_date" : home.end,
                        "ware_house_id" : wStackId
                    }),
                headers : {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (response) {

                if(response.is_error){
                    SweetAlert.swal({
                        title: "",
                        text: response.message
                    },function () {
                        $state.go('login');
                    });
                }
                else{

                    home.turnArray = response.data;
                    ngDialog.open({
                        template: 'turnsModals',
                        className: 'ngdialog-theme-default order-dialogue',
                        showClose: false,
                        scope: $scope,
                        preCloseCallback: function () {
                            return true;
                        }
                    });

                }

            }).error(function (response) {
            });
        }

    }


}

angular
    .module('flyrobe')
    .controller('homeCtrl', homeCtrl);