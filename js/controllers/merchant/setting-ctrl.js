/**
 * Created by Surbhi Goel on 01/02/17.
 */

function settingCtrl($scope,$http,$filter,$state,$localStorage,SweetAlert) {

    if ($localStorage.merchant == "" || $localStorage.merchant == undefined || $localStorage.merchant == null ||
            $localStorage.merchant.access_token == "" || $localStorage.merchant.access_token == undefined || $localStorage.merchant.access_token == null) {
        $state.go('login');
        return;
    }

    set = this;
    set.spinner = true;
    set.bank_Details_Flag = true;
    set.bank_Details = {};

    set.getConsignment      = getConsignment;
    set.getMerchantDetails  = getMerchantDetails;
    set.processFiles        = processFiles;
    set.readImageFile       = readImageFile;
    set.updateLogo          = updateLogo;
    set.createBankDetails       = createBankDetails;

    if(mainVm.startJoyRide){
        set.spinner = false;
    }
    else
        set.getConsignment(); // Default Call


    function getConsignment() {

        $http({
            // url: mainVm.api + 'merchant-consignments/' + $localStorage.merchant.user_details.merchant_id + '/',
            url: mainVm.api + 'merchant-consignments/',
            method: "GET",
            headers: mainVm.header1
        }).success(function (response) {
            set.consignmentData = response.data;
            set.getMerchantDetails();

        }).error(function (response) {
        });

    }


    function getMerchantDetails() {

        $http({
            url: mainVm.api + 'merchants/' + $localStorage.merchant.user_details.merchant_id + '/',
            method: "GET",
            headers: mainVm.header1
        }).success(function (response) {
            set.merchantData = response.data;
            set.bank_Details = response.data.user.userbankdetail_set[0];
            if(response.data.user.userbankdetail_set.length == 0){
                set.bank_Details_Flag = false;
            }
            else{
                set.bank_Details_Flag = true;
            }
            set.spinner = false;

        }).error(function (response) {
        });

    }

    function createBankDetails() {

        var error = false;

        if(set.bank_Details == undefined || set.bank_Details == "" || set.bank_Details == null){
            set.bank_Details = {};
            set.bank_Details.a_name_error = true;
            set.bank_Details.a_number_error = true;
            set.bank_Details.bank_name_error = true;
            set.bank_Details.ifsc_error = true;
            return false;
        }

        if(set.bank_Details.account_holder_name == '' || set.bank_Details.account_holder_name == null || set.bank_Details.account_holder_name == undefined
            || set.bank_Details.account_holder_name.trim() == ''){
            set.bank_Details.a_name_error = true;
            error = true;
        }
        else{
            set.bank_Details.a_name_error = false;
        }

        if(set.bank_Details.account_number == '' || set.bank_Details.account_number == null || set.bank_Details.account_number == undefined
            || set.bank_Details.account_number.trim() == ''){
            set.bank_Details.a_number_error = true;
            error = true;
        }
        else{
            set.bank_Details.a_number_error = false;
        }

        if(set.bank_Details.bank_name == '' || set.bank_Details.bank_name == null || set.bank_Details.bank_name == undefined
            || set.bank_Details.bank_name.trim() == ''){
            set.bank_Details.bank_name_error = true;
            error = true;
        }
        else{
            set.bank_Details.bank_name_error = false;
        }

        if(set.bank_Details.ifsc_code == '' || set.bank_Details.ifsc_code == null || set.bank_Details.ifsc_code == undefined
            || set.bank_Details.ifsc_code.trim() == ''){
            set.bank_Details.ifsc_error = true;
            error = true;
        }
        else{
            set.bank_Details.ifsc_error = false;
        }


        if(error){
            return false;
        }
        else{

             set.spinner = true;

            $http({
                url: mainVm.api + 'user-bank-detail/',
                method: "POST",
                data : {
                    "account_holder_name" : set.bank_Details.account_holder_name,
                    "account_number" : set.bank_Details.account_number,
                    "ifsc_code" : set.bank_Details.ifsc_code,
                    "bank_name" : set.bank_Details.bank_name
                },
                headers: mainVm.header1
            }).success(function (response) {

                if(response.meta.is_error){
                    SweetAlert.swal({
                        title: "",
                        text: "Something Went Wrong"
                    },function () {
                        set.spinner = false;
                    });
                }
                else{
                    SweetAlert.swal({
                        title: "",
                        text: response.meta.message
                    },function () {
                        $state.reload();
                        set.spinner = false;
                    });
                }

            }).error(function (response) {

                SweetAlert.swal({
                    title: "",
                    text: "Something Went Wrong"
                },function () {
                    set.spinner = false;
                });

            });
        }




    }

    function processFiles(file) {

        var fi = file;
        set.spinner = true;
        $scope.$apply();

        if (fi.files.length > 0) {      // FIRST CHECK IF ANY FILE IS SELECTED.

            for (var i = 0; i <= fi.files.length - 1; i++) {
                var fileName, fileExtension, fileSize, fileType, dateModified;

                // FILE NAME AND EXTENSION.
                fileName = fi.files.item(i).name;
                fileExtension = fileName.replace(/^.*\./, '');

                // CHECK IF ITS AN IMAGE FILE.
                // TO GET THE IMAGE WIDTH AND HEIGHT, WE'LL USE fileReader().
                if (fileExtension == 'png' || fileExtension == 'jpg' || fileExtension == 'jpeg') {
                    set.readImageFile(fi.files.item(i));             // GET IMAGE INFO USING fileReader().
                }
            }


        }

    }

    function readImageFile(file) {
        var reader = new FileReader(); // CREATE AN NEW INSTANCE.

        reader.onload = function (e) {
            var img = new Image();
            img.src = e.target.result;

            img.onload = function () {
                var w = this.width;
                var h = this.height;

                if(w > 500 || h > 500){
                    SweetAlert.swal({
                        title: "",
                        text: "The size of the logo image must be constrained to a maximum 120 X 120px"
                    },function () {
                        set.spinner = false;
                    });
                    return false;
                }
                else {
                    var formData = new FormData();
                    if (file) {
                        formData.append('multi_count', 1);
                        formData.append('multi_img_0', file);

                        $.ajax({
                            type: 'POST',
                            url: mainVm.urlList.node_url + 'image-upload-to-cloudinary',
                            dataType: "json",
                            data: formData,
                            async: true,
                            processData: false,
                            contentType: false,
                            success: function (response) {

                                set.updateLogo(response.data[0]);

                                $scope.$apply();

                            },
                            error: function (data) {

                                SweetAlert.swal({
                                    title: "",
                                    text: "Please check your internet & try again."
                                },function () {
                                    set.spinner = false;
                                });

                            }
                        });
                    }
                }
            }
        };
        reader.readAsDataURL(file);
    }

    function updateLogo(file) {

        $.ajax({
            method: "PATCH",
            url: mainVm.api + 'merchants/'+ $localStorage.merchant.user_details.merchant_id + '?platform=dashboard',
            data: JSON.stringify({
                'logo' : mainVm.urlList.imagekit_url + file
            }),
            headers: mainVm.header1,
            success: function (response) {

               if(response.meta.is_error){
                   SweetAlert.swal({
                       title: "",
                       text: "Something went wrong.Please try again later."
                   },function () {
                       set.spinner = false;
                       // $scope.$apply();
                   });
               }
               else{

                   $localStorage.merchant.user_details.logo = response.data.logo;
                   SweetAlert.swal({
                       title: "",
                       text: "Updated"
                   },function () {
                       set.spinner = false;
                       // $scope.$apply();
                   });
               }

            },
            error: function (response, status, headers) {

                SweetAlert.swal({
                    title: "",
                    text: "Something went wrong.Please try again later."
                },function () {
                    set.spinner = false;
                    // $scope.$apply();
                });
            }
        });

    }

}

angular
    .module('flyrobe')
    .controller('settingCtrl', settingCtrl);