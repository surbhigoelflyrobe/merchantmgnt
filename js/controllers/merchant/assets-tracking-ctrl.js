/**
 * Created by Surbhi Goel on 01/02/17.
 */

function assetsTrackingCtrl($scope,$http,$filter,$state,$localStorage,SweetAlert,$timeout) {

    if ($localStorage.merchant == "" || $localStorage.merchant == undefined || $localStorage.merchant == null ||
        $localStorage.merchant.user_details == "" || $localStorage.merchant.user_details == undefined || $localStorage.merchant.user_details == null ||
        $localStorage.merchant.access_token == "" || $localStorage.merchant.access_token == undefined || $localStorage.merchant.access_token == null ||
        $localStorage.merchant.user_details.type.indexOf('C2C') < 0) {
        $state.go('login');
        return;
    }

    atCtrl = this;
    atCtrl.spinner          = true;
    atCtrl.showDetails      = false;
    atCtrl.selectedIndex    = "ABC";

    atCtrl.displayArr = [];

    atCtrl.getAssetList             = getAssetList;
    atCtrl.getDetailForAsset        = getDetailForAsset;
    atCtrl.getOtherDetailForAsset   = getOtherDetailForAsset;
    atCtrl.performActionForFailRCD  = performActionForFailRCD;

    atCtrl.statusArr = {
        ACO : "Asset Accepted",
        PI : "Asset Accepted",
        PSD : "Pickup Scheduled",
        RCD : "Received",
        RPS : "Quality Check Passed",
        QCF : "Quality Check Failed",
        PPS : "Photoshoot & Cataloging",
        UCL : "Photoshoot & Cataloging",
        RTL : "Photoshoot & Cataloging",
        LIV : "Asset is Live",
        RTD : "Asset Return Initiated",
        NRCVD : "Pickup Scheduled"
        // NRCVD : "Not Received"

    };

    atCtrl.queAns = [
        {
            "Q" : "Q. How many days does my asset take to be live on website?",
            "A" : "A. We take 30 days to shoot and make a women's wear item live from the time it reaches our warehouse. We take 45 days to shoot and make a men's wear item live from the time it reaches us "
        },
        {
            "Q" : "Q. My asset has  already taken 30-45 days and is not yet live?",
            "A" : "A. This seems to be a special case. Please email us at curators@flyrobe.com with your asset ID we promise to respond within 24 hours"
        }
    ];

    if(mainVm.startJoyRide){
        atCtrl.showDetails = true;
        atCtrl.spinner     = false;
        atCtrl.selectedDivData          = {};
        atCtrl.selectedDivData.status   = 'PI';
        set.spinner = false;
    }
    else{
        atCtrl.getAssetList(); //Default Call
    }

    function getAssetList() {

        atCtrl.spinner = true;
        atCtrl.showDetails = false;
        atCtrl.merchantId = $localStorage.merchant.user_details.merchant_id;

        $http({
            url: mainVm.urlList.node_url + 'c2c-item-list?vendor_id=' + atCtrl.merchantId,
            method: "GET"
        }).success(function (response) {

            if(response.is_error){

                if(response.status == 101){
                    SweetAlert.swal({
                        title: "",
                        text: response.message
                    },function () {
                        atCtrl.spinner = false;
                    });
                }
                else{
                    SweetAlert.swal({
                        title: "",
                        text: response.message
                    },function () {
                        $state.go('login');
                    });
                }

            }
            else{
                atCtrl.tableData = response.data;
                if(atCtrl.tableData.length){
                    $timeout(function () {
                        atCtrl.getDetailForAsset(atCtrl.tableData[0],0);
                    },100)
                }

                // atCtrl.spinner = false;
            }



        }).error(function (response) {
        });

    }

    function getDetailForAsset(data,index) {

        atCtrl.selectedIndex = index;

        //remove active class from each and every div
        $(".at-asset-details > .at-detail-div").removeClass("at-active-div");
        $(".at-asset-details > .at-img-div").removeClass("at-active-div");

        //assign active class to current div
        $(".at-asset-active-div-" + index + " > .at-detail-div").addClass("at-active-div");
        $(".at-asset-active-div-" + index + " > .at-img-div").addClass("at-active-div");

        atCtrl.showDetails = true;
        atCtrl.selectedDivData = data;
        
        if(data.status == "LIV"){
            atCtrl.spinner = false;
        }
        else{
            atCtrl.spinner = true;
            atCtrl.getOtherDetailForAsset(data);
        }

    }

    function getOtherDetailForAsset(data) {

        atCtrl.displayArr = [];

        $http({
            url: mainVm.urlList.node_url + 'inventory-item-tracking?vendor_id=' + atCtrl.merchantId + '&inventory_id=' + data.id,
            method: "GET"
        }).success(function (response) {

            if(response.is_error){

                if(response.status == 101){
                    SweetAlert.swal({
                        title: "",
                        text: response.message
                    });
                }
                else{
                    SweetAlert.swal({
                        title: "",
                        text: response.message
                    },function () {
                        $state.go('login');
                    });
                }

            }
            else{
                atCtrl.assetDetails = response.data;

                if(response.data.ACO && response.data.ACO != '' && response.data.ACO != null && response.data.ACO != undefined){
                    var temp = {
                        status : 'ACO',
                        pass   : true,
                        success: true,
                        image  : 'img/AssetAccepted@2x.png',
                        date   : response.data.ACO,
                        l1     : false
                    };
                    atCtrl.displayArr.push(temp);
                }

                if(response.data.PSD && response.data.PSD != '' && response.data.PSD != null && response.data.PSD != undefined){
                    atCtrl.displayArr[atCtrl.displayArr.length - 1].l2 = "GREEN";
                    var temp = {
                        status : 'PSD',
                        pass   : true,
                        success: true,
                        image  : 'img/PickupScheduled@2x.png',
                        date   : response.data.PSD,
                        l1     : 'GREEN'
                    };
                    atCtrl.displayArr.push(temp);
                }
                else{
                    atCtrl.displayArr[atCtrl.displayArr.length - 1].l2 = false;
                    var temp = {
                        status : 'PSD',
                        pass   : false,
                        success: false,
                        image  : 'img/PickupScheduled@2x.png',
                        date   : response.data.PSD,
                        l1     : false
                    };
                    atCtrl.displayArr.push(temp);
                }

                if(response.data.RCD || response.data.NRCVD){

                    if(response.data.NRCVD && response.data.NRCVD != '' && response.data.NRCVD != null && response.data.NRCVD != undefined){

                        /*atCtrl.displayArr[atCtrl.displayArr.length - 1].l2 = "RED";
                        var temp = {
                            status : 'NRCVD',
                            pass   : true,
                            success: false,
                            image  : 'img/AssetReceived@2x.png',
                            date   : response.data.NRCVD,
                            l1     : "RED"
                        };
                        atCtrl.displayArr.push(temp);*/
                        atCtrl.spinner = false;
                        return true;
                    }
                    else if(response.data.RCD && response.data.RCD != '' && response.data.RCD != null && response.data.RCD != undefined){
                        atCtrl.displayArr[atCtrl.displayArr.length - 1].l2 = "GREEN";
                        var temp = {
                            status : 'RCD',
                            pass   : true,
                            success: true,
                            image  : 'img/AssetReceived@2x.png',
                            date   : response.data.RCD,
                            l1     : "GREEN"
                        };
                        atCtrl.displayArr.push(temp);
                        atCtrl.performActionForFailRCD(response);
                    }
                    else{
                        atCtrl.performActionForFailRCD(response);
                    }

                }
                else {
                    atCtrl.performActionForFailRCD(response);
                }


                // atCtrl.spinner = false;
            }



        }).error(function (response) {
        });

    }

    function performActionForFailRCD(response) {

        if(!response.data.RCD){
            atCtrl.displayArr[atCtrl.displayArr.length - 1].l2 = false;
            var temp = {
                status : 'RCD',
                pass   : false,
                success: false,
                image  : 'img/AssetReceived@2x.png',
                date   : response.data.RCD,
                l1     : false
            };
            atCtrl.displayArr.push(temp);
        }

        if(response.data.QCF && response.data.QCF != '' && response.data.QCF != null && response.data.QCF != undefined){

            atCtrl.displayArr[atCtrl.displayArr.length - 1].l2 = "RED";
            var temp = {
                status : 'QCF',
                pass   : true,
                success: false,
                image  : 'img/QualityCheck@2x.png',
                date   : response.data.QCF,
                l1     : "RED"
            };
            atCtrl.displayArr.push(temp);

            if(response.data.RTD && response.data.RTD != '' && response.data.RTD != null && response.data.RTD != undefined){
                atCtrl.displayArr[atCtrl.displayArr.length - 1].l2 = "RED";
                var temp = {
                    status : 'RTD',
                    pass   : true,
                    success: false,
                    image  : 'img/AssetReturnInitiated@2x.png',
                    date   : response.data.RTD,
                    l1     : "RED"
                };
                atCtrl.displayArr.push(temp);
            }
            else {
                atCtrl.displayArr[atCtrl.displayArr.length - 1].l2 = false;
                var temp = {
                    status : 'RTD',
                    pass   : false,
                    success: false,
                    image  : 'img/AssetReturnInitiated@2x.png',
                    date   : response.data.RTD,
                    l1     : false
                };
                atCtrl.displayArr.push(temp);
            }

            atCtrl.spinner = false;
            return false;

        }
        else if(response.data.RPS && response.data.RPS != '' && response.data.RPS != null && response.data.RPS != undefined){
            atCtrl.displayArr[atCtrl.displayArr.length - 1].l2 = "GREEN";
            var temp = {
                status : 'RPS',
                pass   : true,
                success: true,
                image  : 'img/QualityCheck@2x.png',
                date   : response.data.RPS,
                l1     : "GREEN"
            };
            atCtrl.displayArr.push(temp);
        }
        else{
            atCtrl.displayArr[atCtrl.displayArr.length - 1].l2 = false;
            var temp = {
                status : 'RPS',
                pass   : false,
                success: false,
                image  : 'img/QualityCheck@2x.png',
                date   : response.data.RPS,
                l1     : false
            };
            atCtrl.displayArr.push(temp);
        }

        if((response.data.PPS && response.data.PPS != '' && response.data.PPS != null && response.data.PPS != undefined) ||
            (response.data.UCL && response.data.UCL != '' && response.data.UCL != null && response.data.UCL != undefined) ||
            (response.data.RTL && response.data.RTL != '' && response.data.RTL != null && response.data.RTL != undefined)){
            atCtrl.displayArr[atCtrl.displayArr.length - 1].l2 = "GREEN";
            var temp = {
                status : 'RTL',
                pass   : true,
                success: true,
                image  : 'img/AssetinPhotoshoot@2x.png',
                date   : response.data.RTL,
                l1     : "GREEN"
            };
            atCtrl.displayArr.push(temp);
        }
        else{
            atCtrl.displayArr[atCtrl.displayArr.length - 1].l2 = false;
            var temp = {
                status : 'RTL',
                pass   : false,
                success: false,
                image  : 'img/AssetinPhotoshoot@2x.png',
                date   : response.data.RTL,
                l1     : false
            };
            atCtrl.displayArr.push(temp);
        }

        atCtrl.displayArr[atCtrl.displayArr.length - 1].l2 = false;
        var temp = {
            status : 'LIV',
            pass   : false,
            success: false,
            image  : 'img/AssetisLive@2x.png',
            date   : response.data.LIV,
            l1     : false
        };
        atCtrl.displayArr.push(temp);

        atCtrl.spinner = false;


    }

}

angular
    .module('flyrobe')
    .controller('assetsTrackingCtrl', assetsTrackingCtrl);