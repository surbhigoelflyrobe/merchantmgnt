/**
 * Created by Surbhi Goel on 01/02/17.
 */

function paymentCtrl($scope,DTOptionsBuilder,$http,$filter,$state,$localStorage,SweetAlert,ngDialog) {

    if ($localStorage.merchant == "" || $localStorage.merchant == undefined || $localStorage.merchant == null ||
            $localStorage.merchant.access_token == "" || $localStorage.merchant.access_token == undefined || $localStorage.merchant.access_token == null) {
        $state.go('login');
        return;
    }

    payment = this;
    payment.selectedTab = 'RECCURING';
    payment.spinner = true;
    payment.tableData = [];

    payment.dtOptions     =   DTOptionsBuilder.newOptions()
        .withDisplayLength(50)
        //.withOption('order', [1, 'asc'])//desc
        .withDOM('<"html5buttons"B>lTfgitp')
        .withButtons([
            {extend: 'copy'}
        ]);

    payment.getData = getData;
    payment.pastData = pastData;
    payment.showAccRjctMsg = showAccRjctMsg;
    payment.changeStatus = changeStatus;
    payment.getAmount = getAmount;

    if(mainVm.startJoyRide){
        payment.spinner = false;
    }
    else
        payment.pastData(); // Default Call

    function getData() {

        payment.spinner = true;

        var data = $.param({
            "user_token" : mainVm.token
        });

        $http({
            url: mainVm.urlList.node_url + 'pending-merchant-payments/',
            method: "POST",
            data: data,
            headers : {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function (response) {

            if(response.is_error){
                SweetAlert.swal({
                    title: "",
                    text: response.message
                },function () {
                    $state.go('login');
                });
            }
            else{
                payment.tableData = response.data;
                payment.spinner = false;
            }

        }).error(function (response) {

            console.log("Error");
            console.log(response);

        });

    }

    function pastData() {

        var data = $.param({
            "user_token" : mainVm.token
        });

        $http({
            url: mainVm.urlList.node_url + 'success-merchant-payments/',
            method: "POST",
            data: data,
            headers : {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function (response) {

            if(response.is_error){
                SweetAlert.swal({
                    title: "",
                    text: response.message
                },function () {
                    $state.go('login');
                });
            }
            else{
                payment.successData = response.data;
                payment.getAmount(data);
            }

        }).error(function (response) {

            console.log("Error");
            console.log(response);

        });

    }

    function getAmount(data) {

        $http({
            url: mainVm.urlList.node_url + 'merchant-current-outstanding-amount/',
            method: "POST",
            data: data,
            headers : {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function (response) {

            if(response.is_error){
                SweetAlert.swal({
                    title: "",
                    text: response.message
                },function () {
                    $state.go('login');
                });
            }
            else{
                payment.currAmount = response.data.current_outstanding_amount;
                payment.getData();
            }

        }).error(function (response) {

            console.log("Error");
            console.log(response);

        });
    }


    function showAccRjctMsg(id,status) {

        payment.changeStatusObj = {
            id : id,
            status : status
        };

        if(status == 'VER'){    //accept
            payment.acceptRejectMsg = "Accepting will rise invoice to flyrobe are you sure you want to continue.";
        }
        else{
            payment.acceptRejectMsg = "Rejecting will not rise invoice to flyrobe are you sure you want to continue.";
        }

        ngDialog.open({
            template: 'accRejctMsgModal',
            className: 'ngdialog-theme-default',
            showClose: false,
            scope: $scope,
            preCloseCallback: function () {
                return true;
            }
        });

    }
    
    
    function changeStatus(id,status) {

        ngDialog.close({
            template: 'accRejctMsgModal',
            className: 'ngdialog-theme-default',
            showClose: false,
            scope: $scope
        });

        var data = $.param({
            "user_token" : mainVm.token,
            "payment_id" : id,
            "status" : status
        });

        $http({
            url: mainVm.urlList.node_url + 'merchant-payments-status-change/',
            method: "POST",
            data: data,
            headers : {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function (response) {

            console.log("success");
            console.log(response);
            if(!response.is_error){
                SweetAlert.swal({
                    title: "",
                    text: response.data
                },function () {
                    $state.reload();
                });
            }
            else{
                SweetAlert.swal({
                    title: "",
                    text: "Something went wrong."
                });
            }

        }).error(function (response) {

            console.log("Error");
            console.log(response);

        });

    }
    
}

angular
    .module('flyrobe')
    .controller('paymentCtrl', paymentCtrl);