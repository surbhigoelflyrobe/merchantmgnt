/**
 * Created by Surbhi Goel on 01/02/17.
 */

function faqCtrl($scope, $http, $filter, $state, $localStorage) {

    if ($localStorage.merchant == "" || $localStorage.merchant == undefined || $localStorage.merchant == null ||
        $localStorage.merchant.access_token == "" || $localStorage.merchant.access_token == undefined || $localStorage.merchant.access_token == null) {
        $state.go('login');
        return;
    }

    fq = this;
    fq.collaspe = collaspe;

    fq.queAns = [
        {
            "Q": "What kind of outfits do you accept?",
            "A": "We accept Bridal Lehengas, Sherwanis, Lehengas, Anarkalis and Gowns. We accept items for lease that are over Rs 25,000 in MRP."
        },
        {
            "Q": "What kind of outfits do well on your platform?",
            "A": "Bridal lehengas, gowns, designer pieces and sherwanis generate the most revenue for our leasing partners"
        },
        {
            "Q": "What do I earn?",
            "A": "We charge 12-15% of the outfit's MRP as rent. Every time an outfit gets rented you make 40-50% depending on your original MRP. GST and Service Tax are deducted from your earnings and any discounts applied by the customer are shared equally. You get paid once an order is fulfilled and not if it is returned or cancelled by the customer."
        },
        {
            "Q": "How do you ensure that quality of my outfits is maintained?",
            "A": " Since the outfit will be rented and used by multiple people over the course of its time with us it will understandably undergo some wear. However, we have a repairs team on staff that takes care of minor damages, repairing embroidery."
        },
        {
            "Q": "What will happen if my outfits get damaged?",
            "A": "A very negligible percentage of our outfits undergo irreparable damage. In that case we compensate you. We pay you 30% of the MRP deducting any past earnings from the garment. For example, if your outfit value is Rs 50,000 then damage payment due to you is Rs 15,000. However, if before getting damaged it has earned Rs 10,000 for you then we owe you Rs 5,000 more. "
        },
        {
            "Q": "Can I order my own outfit if I need it?",
            "A": "Yes. You can book it on the website and mail us your order ID at curators@flyrobe.com we shall apply a code that will make your order free for you. Please note that if the outfit has a prior order then it will not be available for you to book."
        },
        {
            "Q": "How much can I expect to earn?",
            "A": "You can easily expect to earn 15-20% of the MRP of your outfit in the first few months of being live with us."
        },
        {
            "Q": "How much time does it take to shoot the outfit and make it live?",
            "A": "We take 30 days to shoot and make a women's wear item live from the time it reaches our warehouse. We take 45 days to shoot and make a men's wear item live from the time it reaches us"
        },
        {
            "Q": "What happens if my outfit is placed at the store? Can I track orders accurately?",
            "A": "Yes. All bookings at the store are taken via our online systems and will be tracked acccurately on your dashboard. You're welcome to drop by at any of our stores to see the booking process for yourself"
        },
        {
            "Q": "When will my outfit go live?",
            "A": "Thanks for trusting Flyrobe with your outfit. We are excited to help you make money off it. Before it can go live on our website or at stores and serve orders, it needs to go through a few steps. Here are the timelines associated with each: Shipping: The outfit will be shipped to our warehouse via our third party courier service. It will take 7-10 working days to reach us. You can track it using the tracking link provided to you in this duration. Quality Check: We inspect every outfit thoroughly to ensure there are no damages, stains. This will take 5-8 working days from when it reaches us. Shoot and Cataloging: The outfit will be sent for a shoot post quality check. It takes us 20-25 working days from when your outfit passes QC to shoot and make a women's wear outfit live and 40 working days to make a menswear outfit live from when your outfit passes QC.	"
        },
        {
            "Q": "Why does my outfit show temporarily offline on Website?",
            "A": "We periodically review merchandise on the website and remove it if they are not in line with the latest merchandise trends or what our demand is indicating. If your outfit has been made offline, you can choose to request a return for it. We will ship it out 45 days post receiving your request or 45 days after it has served any future order, whichever is later.	"
        },
        {
            "Q": "How do I track the status of my picked up outfit?",
            "A": "You can track the status of your outfit that is in transit to our warehouse by clicking on https://track.shadowfax.in. Enter your Waybill No, select I am returning an item and click Track.	"
        },
        {
            "Q": "My outfit shows damage but I have not been informed",
            "A": "At the outset, we are extremely sorry your outfit has been damaged. If it is any comfort, 80% of our damages are those that can be repaired by our in house team. You have not been informed either since the damage is repairable and we shall soon fix it or because we are still assessing the xtent of the damage. Please rest assured that in the off chance we do find that it's an irreparable damage we shall call you and action a payment per our damage policies here (link)	"
        },
        {
            "Q": "Revenue rental shows x amount but current rental on website is y",
            "A": "We periodically revise the price of our merchandise in line with customer demand and to maximise your earnings for you. If your rental amount is different from the current price on the website it means the price has been revised to your benefit and hence the difference	"
        }
    ];


    function collaspe(index, length) {

        for (var i = 0; i < length; i++) {
            if (i != index) {
                $(".faq-" + i).hide(700);
                $(".faq-sign-" + i).hide();
                $(".faq-sign-plus-" + i).show();
            }
        }

        $(".faq-" + index).toggle(700);
        $(".faq-sign-" + index).toggle();
        $(".faq-sign-plus-" + index).toggle();

    }


}

angular
    .module('flyrobe')
    .controller('faqCtrl', faqCtrl);