/**
 * Created by Surbhi Goel on 01/02/17.
 */

function reportCtrl($scope,DTOptionsBuilder,$http,$filter,$state,$localStorage,SweetAlert) {

    if ($localStorage.merchant == "" || $localStorage.merchant == undefined || $localStorage.merchant == null ||
            $localStorage.merchant.access_token == "" || $localStorage.merchant.access_token == undefined || $localStorage.merchant.access_token == null) {
        $state.go('login');
        return;
    }

    report = this;
}

angular
    .module('flyrobe')
    .controller('reportCtrl', reportCtrl);