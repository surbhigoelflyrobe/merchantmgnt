/**
 ********** Created by Surbhi Goel ************
 **/

function changePwdCtrl($state,$timeout,$localStorage,SweetAlert,$http){

    if ($localStorage.merchant == "" || $localStorage.merchant == undefined || $localStorage.merchant == null
    || $localStorage.merchant.user_details == "" || $localStorage.merchant.user_details == undefined || $localStorage.merchant.user_details == null) {
        $state.go('login');
        return;
    }

    cp = this;
    cp.account = {};
    cp.spinners = false;

    if(!$localStorage.flyrobe){
        $localStorage.flyrobe = {};
        $localStorage.flyrobe.email = "";
        $localStorage.flyrobe.om = {};
    }

    cp.changePassword = changePassword; //function to cp

    /**********************************************
     function to cp
     **********************************************/
    function changePassword(){

        cp.authMsg = '';
        cp.spinners = true;
        cp.dataFlag = true;

        $http({
            method: 'PUT',
            url: mainVm.api + 'merchant-password/' + $localStorage.merchant.user_details.id + '/reset/',
            data: {
                "old_password" : cp.account.oldPassword,
                "new_password" : cp.account.newPassword,
                "re_enter_password" : cp.account.rePassword
            },
            headers: mainVm.header1
        }).success(function (response, status, headers, config) {
            if (!response.meta.is_error) {
                SweetAlert.swal({
                    title: "",
                    text: response.meta.message
                },function () {
                    mainVm.logout();
                });
            }
            freset.spinners = false;
        }).error(function (response, status, headers, config) {
            if(status == 404){
                cp.spinners = false;
                SweetAlert.swal({
                    title: "",
                    text: "Incorrect Password"
                },function () {
                    $state.go('login');
                });
            }
            if (response.meta.is_error) {
                SweetAlert.swal({
                    title: "",
                    text: response.data.old_password
                });
                freset.spinners = false;
            }
        });


    }

}


angular
    .module('flyrobe')
    .controller('changePwdCtrl', changePwdCtrl);