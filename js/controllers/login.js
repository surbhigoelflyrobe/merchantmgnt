/**
 ********** Created by Surbhi Goel ************
 **/

function loginCtrl($state, $timeout, $localStorage, $http, SweetAlert, $scope,ngDialog) {

    login = this;

    login.account = {};
    login.account.client_id = "3BbSCkzwSmoYecGVsGiJxpUCWv3V1B8LL9CH618a";
    login.account.client_secret = "gg5JJSj5XiVLUpqmMPyld7b3inUOVHexKC3J6FUQp22f2kfE9cpF0MRC5XaFCwxVXmirfkb7oVFbu5G5u17DEoqww00Vr9twxWiCWZ9cSLT1wRHKeMA4cIdIopWqQeae";

    login.spinners = false;
    login.forgotSpinners = false;

    $localStorage.merchant = {};

    // if (!$localStorage.merchant) {
    //     $localStorage.merchant = {};
    // }

    login.acceptAgreement       = acceptAgreement; //function to accept agreement
    login.loginToDashboard      = loginToDashboard; //function to login
    login.recover               = recover;


    function acceptAgreement() {

        $http({
            method: 'PATCH',
            url: mainVm.api + 'merchants-admin/' + $localStorage.merchant.user_details.merchant_id + '/',
            data: {
            "is_contract_signed": true
        },
            headers: mainVm.header
        }).success(function (response, status, headers, config) {

            if (response.meta.is_error) {
                SweetAlert.swal({
                    title: "",
                    text: response.meta.message
                });
                login.forgotSpinners = false;
            }
            else{
                $localStorage.merchant.user_details.is_contract_signed = true;
                mainVm.merchant_user_details.is_contract_signed = true;
                mainVm.startTour();
            }

        }).error(function (response, status, headers, config) {

            if (status == 500) {
                SweetAlert.swal({
                    title: "",
                    text: "Something went wrong. Please try again after some time."
                });
                login.forgotSpinners = false;
            }
            else if (response.meta.is_error) {
                SweetAlert.swal({
                    title: "",
                    text: "This e-mail is not registered with us."
                });
                login.forgotSpinners = false;
            }
        });
        
    }
    

    /**********************************************
     function to login
     **********************************************/
    function loginToDashboard() {

        login.authMsg = '';
        login.spinners = true;
        login.dataFlag = true;

        mainVm.header2          = {
            'Authorization' : 'Bearer dashboard',
            'Content-Type'  : 'application/x-www-form-urlencoded'
        };

        $.ajax({
            method: "POST",
            url: mainVm.urlList.url + 'o/token/merchant/',
            data: {
                'grant_type': 'password',
                'username': login.account.email,
                'password': login.account.password,
                'client_id': login.account.client_id,
                'client_secret': login.account.client_secret
            },
            headers: mainVm.header2,
            traditional: true,
            success: function (response) {

                if (response.access_token) {
                    mainVm.token = response.access_token;
                    $localStorage.merchant.access_token = mainVm.token;
                    $localStorage.merchant.user_details = response.user_details;
                    mainVm.merchant_user_details = response.user_details;

                    mainVm.header1.Authorization = 'Bearer ' + $localStorage.merchant.access_token;
                    mainVm.header2.Authorization = 'Bearer ' + $localStorage.merchant.access_token;

                    response.user_details.business_model.forEach(function (col) {
                        mainVm.merchant_user_details.type = mainVm.merchant_user_details.type + "," + col;
                    });

                    $localStorage.merchant.user_details.type =  mainVm.merchant_user_details.type;

                    //tutorial
                    if(mainVm.merchant_user_details.type.indexOf('PRI') >= 0 || mainVm.merchant_user_details.type.indexOf('C2C') >= 0 ||
                        mainVm.merchant_user_details.type.indexOf('B2C') >= 0 || mainVm.merchant_user_details.type.indexOf('B2B') >= 0){
                        var count = 6;
                        $scope.number = 1;
                        if(mainVm.merchant_user_details && mainVm.merchant_user_details.type.indexOf('C2C') >= 0){
                            // var count = 8;
                            count += 2;
                        }
                        if(mainVm.merchant_user_details && mainVm.merchant_user_details.type.indexOf('PTS') >= 0){
                            // var count = 8;
                            count += 5;
                        }

                        mainVm.config = [

                            {
                                type: "location_change",
                                path: "/merchant/home"
                            },
                            {
                                type: "title",
                                heading: "1 of " + count,
                                text: '<div class="row"><div id="title-text" class="col-md-12"><h2>Hello</h2><span class="main-text">Welcome to <strong>Flyrobe\'s Vendor Management System !</strong></span><br><span>' +
                                'We have compiled a quick tutorial that will walk you through the important sections.Please press next to continue or skip to End the tutorial.</span></div></div>',
                                curtainClass: "randomClass"

                            },
                            {
                                type: "element",
                                selector: ".step2",
                                heading: "2 of " + count,
                                text: '<div class="row"><div id="title-text" class="col-md-12">' +
                                '<span class="main-text">' +
                                'Home helps you to understand quick & holistic insights about your business with Flyrobe. Use this section ' +
                                'if you want to understand which are the best/low performing products or if you want a sneak peak into the' +
                                'total revenue generated via Flyrobe till date. ' +
                                '</span></div></div>',
                                placement: "right",
                                curtainClass: "randomClass",
                                attachToBody: true

                            },
                            {
                                type: "location_change",
                                path: "/merchant/orders"
                            },
                            {
                                type: "element",
                                selector: ".step3",
                                heading: "3 of " + count,
                                text: '<div class="row"><div id="title-text" class="col-md-12">' +
                                '<span class="main-text">' +
                                'This section provides you the single view to find all your products listed with Flyrobe along with the ' +
                                'revenue generated per product. Only serviced i.e. completed orders are used to calculate the revenue. ' +
                                '</span></div></div>',
                                placement: "right",
                                curtainClass: "randomClass",
                                attachToBody: true,
                                scroll: true
                            },
                            /* {
                             type: "location_change",
                             path: "/merchant/payment"
                             },
                             {
                             type: "element",
                             selector: ".step4",
                             heading: "4 of " + count,
                             text: '<div class="row"><div id="title-text" class="col-md-12">' +
                             '<span class="main-text">' +
                             'Payments gives you complete view of your past payment history with Flyrobe as well as your upcoming payments.' +
                             'Please note - Invoices are generated only at the end of each payment cycle & can be downloaded for reference.' +
                             '</span></div></div>',
                             placement: "right",
                             curtainClass: "randomClass",
                             attachToBody: true,
                             scroll: true
                             },*/
                            /* {
                             type: "location_change",
                             path: "/merchant/payment"
                             },
                             {
                             type: "element",
                             selector: ".step8",
                             heading: "8 of 8",
                             text: '<div class="row"><div id="title-text" class="col-md-12">' +
                             '<span class="main-text">' +
                             'This section shows the total outstanding receivables from Flyrobe. Invoices of which will be generated ' +
                             'end of each cycle.' +
                             '</span></div></div>',
                             placement: "bottom",
                             curtainClass: "randomClass",
                             attachToBody: true,
                             scroll: true
                             },*/

                        ];

                        $scope.number = 4;

                        if(mainVm.merchant_user_details && mainVm.merchant_user_details.type.indexOf('C2C') >= 0){
                            var temp =  {
                                type: "location_change",
                                path: "/merchant/assets-tracking"
                            };
                            var temp1 = {
                                type: "element",
                                selector: ".step9",
                                heading: $scope.number + " of " + count,
                                text: '<div class="row"><div id="title-text" class="col-md-12">' +
                                '<span class="main-text">' +
                                'This section will help you track the status of items submitted to Flyrobe\'s Make Money Off Your Closet Programme.' +
                                '</span></div></div>',
                                placement: "right",
                                curtainClass: "randomClass",
                                attachToBody: true
                            };
                            $scope.number += 1;
                            var temp2 =  {
                                type: "location_change",
                                path: "/merchant/faq"
                            };
                            var temp3 = {
                                type: "element",
                                selector: ".step10",
                                heading: $scope.number + " of " + count,
                                text: '<div class="row"><div id="title-text" class="col-md-12">' +
                                '<span class="main-text">' +
                                'Your guide to all queries regarding Programme, Payments, Product and more.' +
                                '</span></div></div>',
                                placement: "right",
                                curtainClass: "randomClass",
                                attachToBody: true
                            };
                            $scope.number += 1;
                            mainVm.config.push(temp);
                            mainVm.config.push(temp1);
                            mainVm.config.push(temp2);
                            mainVm.config.push(temp3);
                        }

                        if(mainVm.merchant_user_details && mainVm.merchant_user_details.type.indexOf('PTS') >= 0){
                            var temp = {
                                type: "location_change",
                                path: "/merchant/place-order"
                            };

                            var temp1 = {
                                type: "element",
                                selector: ".step_pts_1",
                                heading: $scope.number + " of " + count,
                                text: '<div class="row"><div id="title-text" class="col-md-12">' +
                                '<span class="main-text">' +
                                'The ‘Place order’ tab will help you record the purchases in the system. You can use it to search for inventory included under Flyrobe Select’s buyback guarantee or to catalogue a new item. ' +
                                '</span></div></div>',
                                placement: "right",
                                curtainClass: "randomClass",
                                attachToBody: true

                            };
                            $scope.number += 1;

                            var temp2 =  {
                                type: "element",
                                selector: ".step_pts_2",
                                heading: $scope.number + " of " + count,
                                text: '<div class="row"><div id="title-text" class="col-md-12">' +
                                '<span class="main-text">' +
                                'Enter the SKU code or design code to know which outfit is included under Flyrobe Select’s buyback.' +
                                '</span></div></div>',
                                placement: "bottom",
                                curtainClass: "randomClass",
                                attachToBody: true

                            };
                            $scope.number += 1;

                            var temp3 = {
                                type: "element",
                                selector: ".step_pts_3",
                                heading: $scope.number + " of " + count,
                                text: '<div class="row"><div id="title-text" class="col-md-12">' +
                                '<span class="main-text">' +
                                ' Add a new item in your inventory by adding the details i.e. SKU id, MRP, outfit Image, and short description of the outfit here.' +
                                '</span></div></div>',
                                placement: "bottom",
                                curtainClass: "randomClass",
                                attachToBody: true

                            };
                            $scope.number += 1;

                            var temp4 = {
                                type: "location_change",
                                path: "/merchant/sales-report"
                            };
                            var temp5 = {
                                type: "element",
                                selector: ".step_pts_4",
                                heading: $scope.number + " of " + count,
                                text: '<div class="row"><div id="title-text" class="col-md-12">' +
                                '<span class="main-text">' +
                                'Sales report helps you review  all the sales made under Flyrobe Select.' +
                                ' You can see a quick snapshot of all the orders along with the total revenue generated. ' +
                                '</span></div></div>',
                                placement: "right",
                                curtainClass: "randomClass",
                                attachToBody: true,
                                scroll: true
                            };
                            $scope.number += 1;

                            var temp6 = {
                                type: "location_change",
                                path: "/merchant/view-inventory"
                            };
                            var temp7 = {
                                type: "element",
                                selector: ".step_pts_5",
                                heading: $scope.number + " of " + count,
                                text: '<div class="row"><div id="title-text" class="col-md-12">' +
                                '<span class="main-text">' +
                                'Save time and view  all the pre listed outfits under Flyrobe Select’s buyback guarantee along with their details.' +
                                '</span></div></div>',
                                placement: "right",
                                curtainClass: "randomClass",
                                attachToBody: true,
                                scroll: true
                            };
                            $scope.number += 1;

                            mainVm.config.push(temp);
                            mainVm.config.push(temp1);
                            mainVm.config.push(temp2);
                            mainVm.config.push(temp3);
                            mainVm.config.push(temp4);
                            mainVm.config.push(temp5);
                            mainVm.config.push(temp6);
                            mainVm.config.push(temp7);
                        }

                        {
                            var temp = {
                                type: "location_change",
                                    path: "/merchant/settings"
                            };
                            var temp1 = {
                                type: "element",
                                    selector: ".step5",
                                heading: $scope.number + " of " + count,
                                text: '<div class="row"><div id="title-text" class="col-md-12">' +
                            '<span class="main-text">' +
                            'You can use this section to change your logo or update your password.' +
                            '</span></div></div>',
                                placement: "right",
                                curtainClass: "randomClass",
                                attachToBody: true,
                                scroll: true
                            };
                            $scope.number += 1;
                            var temp2 = {
                                type: "location_change",
                                    path: "/merchant/home"
                            };
                            var temp3 = {
                                type: "element",
                                    selector: ".step6",
                                heading: $scope.number + " of " + count,
                                text: '<div class="row"><div id="title-text" class="col-md-12">' +
                            '<span class="main-text">' +
                            'This can used to be check product performance during different period of time.' +
                            '</span></div></div>',
                                placement: "bottom",
                                curtainClass: "randomClass",
                                attachToBody: true,
                                scroll: true
                            };
                            $scope.number += 1;
                            var temp4 = {
                                type: "element",
                                    selector: ".step7",
                                heading: $scope.number + " of " + count,
                                text: '<div class="row"><div id="title-text" class="col-md-12">' +
                            '<span class="main-text">' +
                            'Clicking on this will give you details of each order for this product in positive selected time period.' +
                            '</span></div></div>',
                                placement: "bottom",
                                curtainClass: "randomClass",
                                attachToBody: true,
                                scroll: true
                            };
                            $scope.number += 1;
                            var temp5 =  {
                                type: "location_change",
                                path: "/merchant/home"
                            };
                            var temp6 = {
                                type: "title",
                                selector: ".step9",
                                text: '<div class="row"><div id="title-text" class="col-md-12">' +
                                '<span>' +
                                'To view the tutorial again you can click on \'Take a Tour\'. <br> Thank You!' +
                                '</span></div></div>',
                                curtainClass: "randomClass"
                            };
                            mainVm.config.push(temp);
                            mainVm.config.push(temp1);
                            mainVm.config.push(temp2);
                            mainVm.config.push(temp3);
                            mainVm.config.push(temp4);
                            mainVm.config.push(temp5);
                            mainVm.config.push(temp6);
                        }
                    }
                    else{//tutorial for partners-store
                        var count = 7;
                        $scope.number = 1;


                        mainVm.config = [

                            {
                                type: "location_change",
                                path: "/merchant/place-order"
                            },
                            {
                                type: "title",
                                heading: "1 of " + count,
                                text: '<div class="row"><div id="title-text" class="col-md-12"><h2>Hello</h2><span class="main-text">Welcome to <strong>Flyrobe\'s Vendor Management System !</strong></span><br><span>' +
                                'We have compiled a quick tutorial that will walk you through the important sections.Please press next to continue or skip to End the tutorial.</span></div></div>',
                                curtainClass: "randomClass"

                            },
                            {
                                type: "element",
                                selector: ".step_pts_1",
                                heading: "2 of " + count,
                                text: '<div class="row"><div id="title-text" class="col-md-12">' +
                                '<span class="main-text">' +
                                'The ‘Place order’ tab will help you record the purchases in the system. You can use it to search for inventory included under Flyrobe Select’s buyback guarantee or to catalogue a new item. ' +
                                '</span></div></div>',
                                placement: "right",
                                curtainClass: "randomClass",
                                attachToBody: true

                            },
                            {
                                type: "element",
                                selector: ".step_pts_2",
                                heading: "3 of " + count,
                                text: '<div class="row"><div id="title-text" class="col-md-12">' +
                                '<span class="main-text">' +
                                'Enter the SKU code or design code to know which outfit is included under Flyrobe Select’s buyback.' +
                                '</span></div></div>',
                                placement: "bottom",
                                curtainClass: "randomClass",
                                attachToBody: true

                            },
                            {
                                type: "element",
                                selector: ".step_pts_3",
                                heading: "4 of " + count,
                                text: '<div class="row"><div id="title-text" class="col-md-12">' +
                                '<span class="main-text">' +
                                ' Add a new item in your inventory by adding the details i.e. SKU id, MRP, outfit Image, and short description of the outfit here.' +
                                '</span></div></div>',
                                placement: "bottom",
                                curtainClass: "randomClass",
                                attachToBody: true

                            },
                            {
                                type: "location_change",
                                path: "/merchant/sales-report"
                            },
                            {
                                type: "element",
                                selector: ".step_pts_4",
                                heading: "5 of " + count,
                                text: '<div class="row"><div id="title-text" class="col-md-12">' +
                                '<span class="main-text">' +
                                'Sales report helps you review  all the sales made under Flyrobe Select.' +
                                ' You can see a quick snapshot of all the orders along with the total revenue generated. ' +
                                '</span></div></div>',
                                placement: "right",
                                curtainClass: "randomClass",
                                attachToBody: true,
                                scroll: true
                            },
                            {
                                type: "location_change",
                                path: "/merchant/view-inventory"
                            },
                            {
                                type: "element",
                                selector: ".step_pts_5",
                                heading: "6 of " + count,
                                text: '<div class="row"><div id="title-text" class="col-md-12">' +
                                '<span class="main-text">' +
                                'Save time and view  all the pre listed outfits under Flyrobe Select’s buyback guarantee along with their details.' +
                                '</span></div></div>',
                                placement: "right",
                                curtainClass: "randomClass",
                                attachToBody: true,
                                scroll: true
                            },
                            {
                                type: "location_change",
                                path: "/merchant/settings"
                            },
                            {
                                type: "element",
                                selector: ".step5",
                                heading: "7 of " + count,
                                text: '<div class="row"><div id="title-text" class="col-md-12">' +
                                '<span class="main-text">' +
                                'You can use this section to change your logo or update your password and add bank details.' +
                                '</span></div></div>',
                                placement: "right",
                                curtainClass: "randomClass",
                                attachToBody: true,
                                scroll: true
                            },
                            {
                                type: "location_change",
                                path: "/merchant/place-order"
                            },
                            {
                                type: "title",
                                selector: ".step9",
                                text: '<div class="row"><div id="title-text" class="col-md-12">' +
                                '<span>' +
                                'To view the tutorial again you can click on \'Take a Tour\'. <br> Thank You!' +
                                '</span></div></div>',
                                curtainClass: "randomClass"
                            }

                        ];

                    }

                    $localStorage.merchant.config = mainVm.config;


                    SweetAlert.swal({
                        title: "",
                        text: "You are successfully logged in."
                    },function () {
                        // $state.go('merchant.home');

                        if(mainVm.merchant_user_details.type.indexOf('C2C') >= 0 && 
                            mainVm.merchant_user_details.is_contract_signed != true && mainVm.merchant_user_details.is_contract_signed != 'true'){ 
                            ngDialog.open({
                                template: 'agreement',
                                className: 'ngdialog-theme-default agreement-dialogue',
                                showClose: false,
                                scope: $scope,
                                closeByDocument : false
                            });
                        }
                        else{
                            mainVm.startTour();
                        }

                        // mainVm.startTour();

                        /*if(mainVm.merchant_user_details.type.indexOf('PRI') >= 0 || mainVm.merchant_user_details.type.indexOf('C2C') >= 0 ||
                            mainVm.merchant_user_details.type.indexOf('B2C') >= 0 || mainVm.merchant_user_details.type.indexOf('B2B') >= 0){
                            mainVm.startTour();
                        }
                        else if(mainVm.merchant_user_details.type.indexOf('PTS') >= 0){
                            $state.go('merchant.placeOrder');
                        }
                        else{
                            mainVm.startTour();
                        }*/

                    });
                }

            },
            error: function (response, status, headers) {

                if (headers == "Unauthorized") {
                    SweetAlert.swal({
                        title: "",
                        text: "Please Enter Correct Credentials"
                    });
                    login.spinners = false;
                }
                else {
                    SweetAlert.swal({
                        title: "",
                        text: "Something went wrong.Please try again later."
                    });
                    login.spinners = false;
                }
            }
        });


    }

    /**********************************************
     function to forgot pssword
     **********************************************/
    function recover() {

        login.forgotSpinners = true;

        $http({
            method: 'POST',
            url: mainVm.api + 'merchant-password/forgot/',
            data: {
                user_email: login.account.recoverEmail
            },
            headers: mainVm.header1
        }).success(function (response, status, headers, config) {
            if (!response.meta.is_error) {
                login.rSuccessMsg = "Please check the message sent to your registered email & follow the instructions to reset your password.";
                // $timeout(function () {
                //    $state.go('login');
                // },4000);
            }

        }).error(function (response, status, headers, config) {

            if (status == 500) {
                SweetAlert.swal({
                    title: "",
                    text: "Something went wrong. Please try again after some time."
                });
                login.forgotSpinners = false;
            }
            else if (response.meta.is_error) {
                SweetAlert.swal({
                    title: "",
                    text: "This e-mail is not registered with us."
                });
                login.forgotSpinners = false;
            }
        });

    }

}


angular
    .module('flyrobe')
    .controller('loginCtrl', loginCtrl);