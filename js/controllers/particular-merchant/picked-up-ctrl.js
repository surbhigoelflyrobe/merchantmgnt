/**
 * Created by Surbhi Goel on 01/02/17.
 */

function pickedUpCtrl($scope,DTOptionsBuilder,$http,$filter,$state,$localStorage,SweetAlert,ngDialog) {

    if ($localStorage.merchant == "" || $localStorage.merchant == undefined || $localStorage.merchant == null ||
        $localStorage.merchant.access_token == "" || $localStorage.merchant.access_token == undefined || $localStorage.merchant.access_token == null) {
        $state.go('login');
        return;
    }

    pkCtrl = this;
    pkCtrl.spinner = false;

    mainVm.ctlr = 'PICKEDUP';
    pkCtrl.date = $filter('date')(new Date(),'dd-MMM-yyyy');

    pkCtrl.getData = getData;

    pkCtrl.getData(); //Default Call

    pkCtrl.dtOptions     =   DTOptionsBuilder.newOptions()
        .withDisplayLength(50)
        //.withOption('order', [1, 'asc'])//desc
        .withDOM('<"html5buttons"B>lTfgitp')
        .withButtons([
            {extend: 'copy'}
        ]);


    function getData(){

        pkCtrl.spinner = true;

        $http({
            method: 'GET',
            url: mainVm.urlList.merchant_url + "items-list-to-be-shift-on-date?on_date=" +  new Date(pkCtrl.date) + "&access_token=" + $localStorage.merchant.access_token,
            headers: mainVm.headers
        }).success(function (data) {

            pkCtrl.spinner = false;
            pkCtrl.tableData = [];

            data.data.forEach(function (col) {

                col.delivery_date = $filter('date')(new Date(col.delivery_date),'dd MMM yyyy');
                col.created_on = $filter('date')(new Date(col.created_on),'dd MMM yyyy');

                col.image.replace('upload','upload/w_100');
                pkCtrl.tableData.push(col);

            });


        });

    }

}

angular
    .module('flyrobe')
    .controller('pickedUpCtrl', pickedUpCtrl);