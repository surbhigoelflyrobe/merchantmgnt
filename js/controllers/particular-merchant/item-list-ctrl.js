/**
 * Created by Surbhi Goel on 01/02/17.
 */

function itemListCtrl($scope, DTOptionsBuilder, $http, $filter, $state, $localStorage, SweetAlert, ngDialog,
                      checkService) {

    if ($localStorage.merchant == "" || $localStorage.merchant == undefined || $localStorage.merchant == null ||
        $localStorage.merchant.access_token == "" || $localStorage.merchant.access_token == undefined || $localStorage.merchant.access_token == null) {
        $state.go('login');
        return;
    }

    ilCtrl = this;
    ilCtrl.spinner = false;

    ilCtrl.categoryArr = [];
    ilCtrl.tableData = [];
    ilCtrl.unavailableItemArr = [];
    ilCtrl.allowedCate = [4, 9, 10, 11, 12, 14, 16, 17];

    mainVm.ctlr = 'ITEM-LIST';

    ilCtrl._4_plus_month = new Date();
    ilCtrl._4_plus_month.setMonth(ilCtrl._4_plus_month.getMonth() + 4);
    ilCtrl._4_plus_month = $filter('date')(new Date(ilCtrl._4_plus_month), 'yyyy-MM-dd');

    ilCtrl.getCategory = getCategory;
    ilCtrl.getData                      = getData;
    ilCtrl.viewOrders                   = viewOrders;
    ilCtrl.openModalForBlockDate        = openModalForBlockDate;
    ilCtrl.blockDates                   = blockDates;
    ilCtrl.changeStartDate              = changeStartDate;
    ilCtrl.cancelOrder                  = cancelOrder;
    ilCtrl.selectCheckbox               = selectCheckbox;
    ilCtrl.markItemsUnavailble          = markItemsUnavailble;
    ilCtrl.openModalMarkItemsUnavailble = openModalMarkItemsUnavailble;

    ilCtrl.dtOptions = DTOptionsBuilder.newOptions()
        .withDisplayLength(50)
        //.withOption('order', [1, 'asc'])//desc
        .withDOM('<"html5buttons"B>lTfgitp')
        .withButtons([
            {extend: 'copy'}
        ]);

    ilCtrl.getCategory();//Default Call


    function getCategory() {

        ilCtrl.spinner = true;

        $http({
            method: 'GET',
            url: mainVm.api + "inventory-categories/",
            headers: mainVm.header
        }).success(function (data) {

            ilCtrl.categoryArr = [];
            data.data.forEach(function (col) {
                if (ilCtrl.allowedCate.indexOf(col.id) >= 0) {
                    ilCtrl.categoryArr.push(col);
                }
            });

            var temp = {
                id: -1,
                name: "All"
            };
            ilCtrl.category = temp; //Default set
            ilCtrl.categoryArr.push(temp);
            ilCtrl.getData();
        });

    }

    function getData() {

        ilCtrl.spinner = true;
        ilCtrl.unavailableItemArr = [];

        $http({
            method: 'GET',
            url: mainVm.urlList.merchant_url + "consignment-list?category_id=" + ilCtrl.category.id +
            "&access_token=" + $localStorage.merchant.access_token
        }).success(function (data) {

            if(data.is_error){
                SweetAlert.swal({
                    title: "",
                    text: data.message
                });
            }
            else{
                ilCtrl.tableData = data.data;
                ilCtrl.spinner = false;
            }

        });

    }

    function viewOrders(itemId) {

        ilCtrl.spinner = true;

        $http({
            method: 'GET',
            url: mainVm.urlList.merchant_url + "inventory-item-blocked-period?item_id=" + itemId
        }).success(function (data) {

            ilCtrl.itemDataArr = data.data;
            ilCtrl.spinner = false;

            ngDialog.open({
                template: 'viewOrdersModal',
                className: 'ngdialog-theme-default il-view-order-dialog',
                showClose: true,
                scope: $scope,
                preCloseCallback: function () {
                    return true;
                }
            });

        });

    }

    function openModalForBlockDate(itemId, id) {

        ilCtrl.spinner = true;
        ilCtrl.block_item_id = itemId;
        ilCtrl.unique_item_id = id;

        $http({
            method: 'GET',
            url: mainVm.urlList.merchant_url + "inventory-item-blocked-period?item_id=" + itemId
        }).success(function (data) {

            ilCtrl.itemDataArr = data.data;
            ilCtrl.spinner = false;
            ilCtrl.disabledStartDatesArr = [];

            ilCtrl.itemDataArr.forEach(function (dd) {
                if (dd) {
                    dd.blocked_period.forEach(function (col) {
                        var date = new Date(col);
                        var t = new Date(date.getMonth() + 1 + '/' + date.getDate() + '/' + date.getFullYear());
                        ilCtrl.disabledStartDatesArr.push(t.getTime());
                    });
                }
            });

            ilCtrl.startDate = "";
            ilCtrl.endDate = "";
            ilCtrl.deliveryDate = "";

            ngDialog.open({
                template: 'blockDateModal',
                className: 'ngdialog-theme-default il-block-date-dialog',
                showClose: true,
                scope: $scope,
                preCloseCallback: function () {
                    return true;
                }
            });

        });

    }

    function blockDates() {

        var error = false;

        if(checkService.daysBetween(new Date(ilCtrl._4_plus_month),new Date(ilCtrl.startDate)) >= 0){
            ilCtrl.sdate_error_flag = true;
            error = true;
        }
        else {
            ilCtrl.sdate_error_flag = false;
        }

        if(checkService.daysBetween(new Date(ilCtrl._4_plus_month),new Date(ilCtrl.endDate)) > 0){
            ilCtrl.edate_error_flag = true;
            error = true;
        }
        else {
            ilCtrl.edate_error_flag = false;
        }

        if(!error){
            ngDialog.close({
                template: 'blockDateModal',
                className: 'ngdialog-theme-default',
                showClose: true,
                scope: $scope,
                preCloseCallback: function () {
                    return true;
                }
            });
            ilCtrl.spinner = true;


            $http({
                url: mainVm.api + "pufm-blocked-period/",
                method: "POST",
                data: {
                    "inventory": ilCtrl.unique_item_id,
                    "start_date": ilCtrl.startDate,
                    "end_date": ilCtrl.endDate,
                    "delivery_date": ilCtrl.deliveryDate
                },
                headers: mainVm.header
            }).success(function (response) {

                if (response.meta.is_error == true) {
                    SweetAlert.swal({
                        title: "",
                        text: response.meta.message
                    }, function () {
                        ilCtrl.spinner = false;
                        ngDialog.open({
                            template: 'blockDateModal',
                            className: 'ngdialog-theme-default il-block-date-dialog',
                            showClose: true,
                            scope: $scope,
                            preCloseCallback: function () {
                                return true;
                            }
                        });
                    });
                }
                else {
                    SweetAlert.swal({
                        title: "",
                        text: "Blocked successfully"
                    }, function () {
                        $state.reload();
                    });
                }

            });
        }


    }

    function changeStartDate(date) {

        var date = new Date(date);
        var t = new Date(date.getMonth() + 1 + '/' + date.getDate() + '/' + date.getFullYear());
        var time = t.getTime();
        var diff = 0;

        //calculate max-value for end-date
        for (var i = ilCtrl.itemDataArr.length - 1; i >= 0; i--) {

            if (ilCtrl.itemDataArr[i]) {
                for (var j = 0; j < ilCtrl.itemDataArr[i].blocked_period.length; j++) {

                    var date = new Date(ilCtrl.itemDataArr[i].blocked_period[j]);
                    var t = new Date(date.getMonth() + 1 + '/' + date.getDate() + '/' + date.getFullYear());
                    var t = t.getTime();

                    if (diff == 0 && time < t) {
                        diff = t - time;
                    }

                }
            }
        }

        var total_time = parseInt(time) + parseInt(diff);
        ilCtrl.lastDate = new Date(parseInt(time) + parseInt(diff));

        if (total_time == time) {
            // ilCtrl.lastDate = ilCtrl._4_plus_month;
            ilCtrl.lastDate = '';
        }
        else {
            ilCtrl.lastDate.setDate(ilCtrl.lastDate.getDate() - 1);
            ilCtrl.lastDate = $filter('date')(new Date(ilCtrl.lastDate), 'yyyy-MM-dd');
        }

    }

    function cancelOrder(id) {

        ngDialog.close({
            template: 'viewOrdersModal',
            className: 'ngdialog-theme-default il-view-order-dialog',
            showClose: true,
            scope: $scope,
            preCloseCallback: function () {
                return true;
            }
        });

        $http({
            url: mainVm.api + "pufm-blocked-period/" + id + "/",
            method: "PATCH",
            data: {
                "is_active": false
            },
            headers: mainVm.header
        }).success(function (response) {

            if (response.meta.is_error == true) {
                SweetAlert.swal({
                    title: "",
                    text: response.meta.message
                }, function () {
                    ilCtrl.spinner = false;
                    ngDialog.open({
                        template: 'viewOrdersModal',
                        className: 'ngdialog-theme-default il-view-order-dialog',
                        showClose: true,
                        scope: $scope,
                        preCloseCallback: function () {
                            return true;
                        }
                    });
                });
            }
            else {
                SweetAlert.swal({
                    title: "",
                    text: "Order cancel successfully"
                }, function () {
                    $state.reload();
                });
            }

        }).error(function (response, header) {

            if (header == 404) {
                SweetAlert.swal({
                    title: "",
                    text: "Not Found"
                }, function () {
                    ilCtrl.spinner = false;
                    ngDialog.open({
                        template: 'viewOrdersModal',
                        className: 'ngdialog-theme-default il-view-order-dialog',
                        showClose: true,
                        scope: $scope,
                        preCloseCallback: function () {
                            return true;
                        }
                    });
                });
            }

        });

    }

    function selectCheckbox(data) {

        if (data.checked) {
            data.checked = false;
            var i = ilCtrl.unavailableItemArr.indexOf(data.item_id);
            if (i != -1) {
                ilCtrl.unavailableItemArr.splice(i, 1);
            }
        }
        else {
            data.checked = true;
            ilCtrl.unavailableItemArr.push(data.item_id)
        }

    }

    function openModalMarkItemsUnavailble() {

        if (ilCtrl.unavailableItemArr.length <= 0) {
            SweetAlert.swal({
                title: "",
                text: "Please select atleast one item."
            });
        }
        else {

            ngDialog.open({
                template: 'unavailableMsgModal',
                className: 'ngdialog-theme-default msg-dialog',
                scope: $scope,
                preCloseCallback: function () {
                    return true;
                }
            });

        }

    }

    function markItemsUnavailble() {

        ngDialog.close({
            template: 'unavailableMsgModal',
            className: 'ngdialog-theme-default msg-dialog',
            scope: $scope,
            preCloseCallback: function () {
                return true;
            }
        });

        $http({
            url: mainVm.api + "pufm-unavailable-item",
            method: "POST",
            data: {
                "item_ids": ilCtrl.unavailableItemArr,
                "reason_out_of_stock_id": 20
            },
            headers: mainVm.header
        }).success(function (response) {

            if (response.meta.is_error == true) {
                SweetAlert.swal({
                    title: "",
                    text: response.meta.message
                }, function () {
                    ilCtrl.spinner = false;
                });
            }
            else {
                SweetAlert.swal({
                    title: "",
                    text: response.data
                }, function () {
                    $state.reload();
                });
            }

        }).error(function (response, header) {

            if (header == 404) {
                SweetAlert.swal({
                    title: "",
                    text: "Not Found"
                }, function () {
                    ilCtrl.spinner = false;
                    ngDialog.open({
                        template: 'viewOrdersModal',
                        className: 'ngdialog-theme-default il-view-order-dialog',
                        showClose: true,
                        scope: $scope,
                        preCloseCallback: function () {
                            return true;
                        }
                    });
                });
            }

        });

    }

}

angular
    .module('flyrobe')
    .controller('itemListCtrl', itemListCtrl);