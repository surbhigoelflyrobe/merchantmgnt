/**
 * Created by Surbhi Goel on 01/02/17.
 */

function droppedOffCtrl($scope,DTOptionsBuilder,$http,$filter,$state,$localStorage,SweetAlert,ngDialog,
                        checkService) {

    if ($localStorage.merchant == "" || $localStorage.merchant == undefined || $localStorage.merchant == null ||
        $localStorage.merchant.access_token == "" || $localStorage.merchant.access_token == undefined || $localStorage.merchant.access_token == null) {
        $state.go('login');
        return;
    }

    doCtrl = this;
    doCtrl.spinner = false;

    mainVm.ctlr = 'DROP-OFF';
    doCtrl.date = $filter('date')(new Date(),'dd-MMM-yyyy');

    doCtrl.getData = getData;

    doCtrl.getData(); //Default Call

    doCtrl.dtOptions     =   DTOptionsBuilder.newOptions()
        .withDisplayLength(50)
        //.withOption('order', [1, 'asc'])//desc
        .withDOM('<"html5buttons"B>lTfgitp')
        .withButtons([
            {extend: 'copy'}
        ]);


    function getData(){

        doCtrl.spinner = true;

        $http({
            method: 'GET',
            url: mainVm.urlList.merchant_url + "items-list-to-be-drop-on-date?on_date=" +  new Date(doCtrl.date) + "&access_token=" + $localStorage.merchant.access_token,
            headers: mainVm.headers
        }).success(function (data) {

            doCtrl.spinner = false;
            doCtrl.tableData = [];

            data.data.forEach(function (col) {

                if(col.pickup_date){
                    col.pickup_date = $filter('date')(new Date(col.pickup_date),'dd MMM yyyy');
                }
                else{
                    col.pickup_date = "-";
                }

                if(col.created_on){
                    col.created_on = $filter('date')(new Date(col.created_on),'dd MMM yyyy');
                }
                else{
                    col.created_on = "-";
                }

                if(col.next_order){
                    col.next_order = $filter('date')(new Date(col.next_order),'dd MMM yyyy');
                }
                else{
                    col.next_order = "-";
                }

                col.image.replace('upload','upload/w_100');
                doCtrl.tableData.push(col);

            });


        });

    }

}

angular
    .module('flyrobe')
    .controller('droppedOffCtrl', droppedOffCtrl);