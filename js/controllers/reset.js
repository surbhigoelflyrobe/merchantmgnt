/**
 ********** Created by Surbhi Goel ************
 **/

function forgotResetCtrl($state, $timeout, $localStorage, $http, SweetAlert, $scope,$stateParams) {

    freset = this;

    freset.id = $stateParams.userId;
    freset.spinners = false;
    freset.account = {};
    freset.mailAcc = {};

    freset.resetPassword = resetPassword; // reset password after forgot password
    freset.resetEmailPassword = resetEmailPassword; // reset password after first login

    function resetPassword() {

        freset.spinners = true;

        $http({
            method: 'PUT',
            url: mainVm.api + 'merchant-password/' + freset.id + '/verify/',
            data: {
                "new_password" : freset.account.newPassword,
                "re_enter_password" : freset.account.rePassword
            },
            headers: mainVm.header1
        }).success(function (response, status, headers, config) {
            if (!response.meta.is_error) {
                SweetAlert.swal({
                    title: "",
                    text: response.meta.message
                },function () {
                    $state.go('login');
                });
            }
            freset.spinners = false;
        }).error(function (response, status, headers, config) {
            if (response.meta.is_error) {
                SweetAlert.swal({
                    title: "",
                    text: response.data.old_password
                });
                freset.spinners = false;
            }
        });


    }

    function resetEmailPassword() {

        freset.spinners = true;

        $http({
            method: 'PUT',
            url: mainVm.api + 'merchant-password/' + freset.id + '/reset/',
            data: {
                "old_password" : freset.mailAcc.oldPassword,
                "new_password" : freset.mailAcc.newPassword,
                "re_enter_password" : freset.mailAcc.rePassword
            },
            headers: mainVm.header1
        }).success(function (response, status, headers, config) {
            if (!response.meta.is_error) {
                SweetAlert.swal({
                    title: "",
                    text: response.meta.message
                },function () {
                    $state.go('login');
                });
            }
            freset.spinners = false;
        }).error(function (response, status, headers, config) {
            if(status == 404){
                SweetAlert.swal({
                    title: "",
                    text: "This user doesn't exist with us."
                },function () {
                    $state.go('login');
                });
            }
            else if (status == 400) {
                SweetAlert.swal({
                    title: "",
                    text: response.data.user
                });
                freset.spinners = false;
            }
            else if (response.meta.is_error) {
                SweetAlert.swal({
                    title: "",
                    text: response.data.old_password
                });
                freset.spinners = false;
            }
        });


    }

}

angular
    .module('flyrobe')
    .controller('forgotResetCtrl', forgotResetCtrl);